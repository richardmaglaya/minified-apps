imageStore = {
    pick: function(callback) {
        filepicker.pick({
            mimetypes: ['image/*'],
            services: ['COMPUTER'],
            openTo: 'COMPUTER'
        }, function(blob){
            filepicker.stat(blob, {width: true, height: true}, function(metadata){
                callback(blob, metadata);
            });
        });
    },
    store: function(blob, callback) {
        filepicker.store(blob, {
            mimetype: blob.mimetype,
            path: '/collateral/',
            access: 'public',
            location: 'S3'
        }, function(new_blob) {
            callback(new_blob);
            filepicker.remove(blob, {});
        });
    },
    remove: function(blob, callback) {
        filepicker.remove(blob, {}, callback);
    }
};

