$(document).ready(function () {
    $('[data-confirmation-for]').each(function(index, el) {
        var confirmationElement = $(el);
        var label = $('<span class="label"><i></i></span>');
        confirmationElement.after(label);
        var icon = label.children('i');
        var targetElement = $(confirmationElement.data('confirmation-for'));
        var compare = function() {
            $('.help-inline', confirmationElement.parent()).hide();
            if (targetElement.val() != confirmationElement.val())
                icon.removeClass('icon-ok-sign success').addClass('icon-remove-sign error');
            else
                icon.removeClass('icon-remove-sign error').addClass('icon-ok-sign success');
        };
        confirmationElement.keyup(compare);
        targetElement.keyup(compare);
    });
});