$(document).ready(function () {
    $('[data-toggle-language]').change(function(){
        if ($(this).is(':checked')) {
            $($(this).data('target'), $(this).closest($(this).data('parent'))).toggle();
        }
    });

    $('.modal-code-snippet').on('show', function(){
        var modal =$(this);
        setTimeout(function() {
            $('textarea:visible', modal).select();
        }, 1);
    });

});