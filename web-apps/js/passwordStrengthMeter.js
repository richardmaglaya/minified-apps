$(document).ready(function () {
    $('[data-password-strength-meter]').before('<span class="password-strength-meter label"></span>');
    $('[data-password-strength-meter]').after('<span class="password-strength-verdict label"></span>');
    $('[data-password-strength-meter]').after('<span class="password-strength-errors label"></span>');
    $('[data-password-strength-meter]').pwstrength({
        minChar: 8,
        errorMessages: {
            password_to_short: message("global.password.weak")
        },
        scores: [17, 17, 26, 50],
        verdicts: [
            "<span class='error'>"+ message("global.password.weak") +"</span>",
            "<span class='error'>"+ message("global.password.weak") +"</span>",
            "<span class='warning'>"+ message("global.password.good") +"</span>",
            "<span class='warning'>"+ message("global.password.good") +"</span>",
            "<span class='success'>"+ message("global.password.strong") +"</span>"
        ],
        showVerdicts: true,
        viewports: {
            progress: '.password-strength-meter',
            verdict: '.password-strength-verdict',
            errors: '.password-strength-errors'
        },
        onLoad: function() {
            $('[data-password-strength-meter]').each(function(index, element) {
                $('.password-strength-verdict', $(element).parent()).hide();
            });
        },
        onKeyUp: function (evt) {
            $(evt.target).pwstrength("outputErrorList");
            var verdict = $('.password-strength-verdict', $(evt.target).parent());
            if ($(evt.target).data('pwstrength').errors.length > 0) {
                verdict.hide();
            } else {
                verdict.show();
            }
            $('.help-inline', $(evt.target).parent()).hide();
        }
    });
    $('[data-password-strength-meter]').pwstrength("addRule", "oneNumberAndOneSpecial", function (options, word, score) {
        var hasLetters = word.match(/[a-zA-Z]+/);
        var hasNumbers = word.match(/[0-9]+/);
        var hasSpecialCharacters = word.match(/[!,@,#,$,%,\^,&,*,?,_,~]+/);
        var ruleScore = hasLetters && hasNumbers && hasSpecialCharacters && score;
        if (!ruleScore && options.errors.length == 0) {
            options.errors.push(message("global.password.tooSimple"));
        }
        return ruleScore;
    }, 10, true);
});

