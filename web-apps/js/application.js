
AjaxUnknownError = {
    IGNORE : 0,
    ALERT : 1,
    ERROR_PAGE : 2
}

function handleAjaxError(xhr, ajaxUnknownError) {
    if (ajaxUnknownError === undefined) {
        ajaxUnknownError = AjaxUnknownError.IGNORE;
    }
    if (xhr !== undefined) {
        switch (xhr.status) {
            case 401:
            case 403:
                window.location = appBaseUrl;
                break;
            case 422:
                //Duplicate Collateral
                if(xhr.responseText != null){
                    alert(xhr.responseText);
                }
                break;
            default:
                switch (ajaxUnknownError) {
                    case 0: break;
                    case 1:
                        alert(message("global.ajax.alert.error"));
                        break;
                    case 2:
                        //TODO: generic error page?
                        //window.location = '';
                        break;
                    default:
                }
        }
    }
}

function setupBtnSingleClick(){
    $("form .btn-single-click ").closest("form").submit(function(){
        $('.btn-single-click', this).attr('disabled', 'disabled');
        return true;
    });
}

function setupBtnExternalSubmit(){
    $(".btn-external-submit").click(function(){
        $(this).attr('disabled', 'disabled');
        $($(this).data("target")).submit()
    });
}

function setupPhoneNumberSubmit(){
    $("form .phoneNumberFormat ").closest("form").submit(function(){
        var phoneNumber = $('input.phoneNumberFormat', this)
        phoneNumber.val(stripNonNumeric(phoneNumber.val()))
    });
}

// This function removes non-numeric characters
function stripNonNumeric( str )
{
    str += '';
    var rgx = /^\d|\.|-$/;
    var out = '';
    for( var i = 0; i < str.length; i++ )
    {
        if( rgx.test( str.charAt(i) ) ){
            if( !( ( str.charAt(i) == '.' && out.indexOf( '.' ) != -1 ) ||
                ( str.charAt(i) == '-' && out.length != 0 ) ) ){
                out += str.charAt(i);
            }
        }
    }
    return out;
}

function message(){
    var args = Array.prototype.slice.call(arguments, 0);
    var key = args[0];
    return MESSAGES[key];
}

$(document).ready(function(){
    setupBtnSingleClick();
    setupBtnExternalSubmit();
    setupPhoneNumberSubmit();
});