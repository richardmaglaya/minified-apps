$(document).ready(function() {
    setupGridAjax();
});

function setupGridAjax() {
    $(".grid").find(".pagination a, th.sortable a").live('click', function(event) {
        event.preventDefault();
        var url = $(this).attr('href');
        var grid = $(this).closest('.grid');
        grid.fadeOut('fast');
        $.ajax({
            type: 'GET',
            url: url,
            success: function(data) {
                grid.html(data).fadeIn('slow');
            },
            error: function(xhr) {
                handleAjaxError(xhr, AjaxUnknownError.ERROR_PAGE)
            }
        })
    });
}