$(document).ready(function () {
    $(".expiryDateFormat").each(function(){
        $(this).attr("placeholder", "MM/YYYY");
        $(this).mask("99/9999");
    });
    $(".phoneNumberFormat").each(function(){
        $(this).attr("placeholder", "(___) ___ - ____");
        $(this).mask("(999) 999-9999");
    });
    $(".dateFormat").each(function(){
        $(this).attr("placeholder", "DD/MM/YYYY");
        $(this).mask("99/99/9999");
    });
});
