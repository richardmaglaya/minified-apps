$(document).ready(function () {

    refreshToolContainer();

    $('.partner-tool-container input:radio').click(function(){
        refreshToolContainer();
    });

    $('input', '#view-form.disabled').attr("disabled", "disabled");
    $('select', '#view-form.disabled').attr("disabled", "disabled");
});

function refreshToolContainer() {
    $('.partner-tool-container input:radio:checked').each(function(){
        var enabled = ($(this).val() != '');
        var container = $(this).closest('.partner-tool-container');
        var options = $('input:checkbox', container);
        if (!enabled && !container.hasClass('disabled')) {
            container.addClass('disabled');
            options.attr('disabled', true);
        }

        if (enabled && container.hasClass('disabled')) {
            container.removeClass('disabled');
            options.attr('disabled', false);
        }
    });
}
