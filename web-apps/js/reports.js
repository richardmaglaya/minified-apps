var Reports = Reports || {
    decorateWithWidgetStyle: function(obj) {
        var decoratedObj = {
            'color': '#000',
            'number-background-color': '#ecedee',
            'label-background-color': '#ecedee'
        }
        if (obj) {
            for (var key in obj) {
                decoratedObj[key] = obj[key];
            }
        }
        return decoratedObj;
    }
};

/**
 * This method monkey-patches the PieChart widget so that we can add custom formatting to the labels in the
 * tooltips.
 *
 * Except where indicated, this is almost entirely copy and pasted from the Keen.io javascript source file
 *
 *
 * @param params some google visualization params to customize the pie chart display
 */
var overridePieChartDrawing = function(params) {
    // See https://developers.google.com/chart/interactive/docs/gallery/piechart#Data_Format
    // for the available style options
    if (!('tooltipTextStyle' in params)) params.tooltipTextStyle = { fontSize: 10 };
    if (!('pieSliceTextStyle' in params)) params.pieSliceTextStyle = { fontSize: 12 };
    Keen.PieChart.prototype.draw = function(element, response){

        element.innerHTML = "";

        //Apply the width to the div so that it takes up the correct amount of space from the beginning
        element.style.width = (this.options.width + "px");
        element.style.height = (this.options.height + "px");
        element.style.display = "block";

        Keen.showLoading(element);

        var convertOptions = function(opts){
            var options = {};
            options.legend = {};
            options.height = opts.height;
            options.width = opts.width;
            options.title = opts.title;
            options.sliceVisibilityThreshold = (opts.minimumSlicePercentage * .01);
            options.colors = opts.colors;
            options.backgroundColor = opts.backgroundColor;
            if(!opts.showLegend){
                options.legend["position"] = "none";
            }
            options.fontName = opts.font;
            options.titleTextStyle = {color: opts.fontColor};
            options.legend.textStyle = {color: opts.fontColor};
            options.tooltip = {};
            /* Start custom styling for TU Group */
            options.tooltip.textStyle = params.tooltipTextStyle;
            options.pieSliceTextStyle = params.pieSliceTextStyle;
            /* End custom styling for TU Group */
            options["chartArea"] = {
                left: opts.chartAreaLeft,
                top: opts.chartAreaTop,
                height: opts.chartAreaHeight,
                width: opts.chartAreaWidth
            };

            return options;
        };

        var drawIt = _.bind(function(response){

            this.data = response.result;

            var dataTable = new google.visualization.DataTable();
            dataTable.addColumn("string", "Group By");
            dataTable.addColumn("number", this.getLabel());
            _.each(this.data, function(item) {

                var name = "";

                if(this.options.labelMapping[item[this.query.attributes.groupBy]] != null){
                    name = this.options.labelMapping[item[this.query.attributes.groupBy]];
                }
                else{
                    name = item[this.query.attributes.groupBy] + "";
                }

                var value = item.result;

                dataTable.addRow([name, value]);

            }, this);


            if(google){
                /*
                 * Start customization for TU Group
                 */
                var formatter = new google.visualization.NumberFormat({
                    fractionDigits: 2,
                    prefix: '$'
                });

                formatter.format(dataTable, 1); // Apply formatter to second column.

                /*
                 * End customization for TU Group
                 */

                var chart = new google.visualization.PieChart(element);
                chart.draw(dataTable, convertOptions(this.options));

            }
            else{
                console.log("Charting is not yet ready.  Are you waiting for onChartsReady?");
            }

        }, this);

        if(_.isUndefined(response)){
            this.query.getResponse(drawIt);
        }
        else{
            drawIt(response);
        }
    };
};
