$(document).ready(function () {

    var originalList = $('#stateOrProvince').clone();

    $('[data-state-or-province]').each(function(index, radio) {
        var refresh = function(radio) {
            if (radio.is(':checked')) {
                var target = $(radio.data('target'));
                var country = radio.val();
                target.empty().append($(originalList).html());
                target.removeAttr('disabled');
                target.find('option').each(function(i, o) {
                    var state = $(o).attr('value');
                    if (state != '') {
                        if (state.indexOf(country + '-') != 0) {
                            if ($(o).attr('selected') != null){
                                target.children('option').first().attr('selected', 'selected');
                            }
                            $(o).remove();
                        }
                    }
                })
            }
        }
        refresh($(radio))
        $(radio).click(function(){
            refresh($(radio))
        })
    });
});
