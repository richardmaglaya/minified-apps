var MESSAGES = {
    "global.ajax.alert.error": "Désolé, une erreur s'est produite lors du traitement de votre demande.",
    "global.password.weak": "Faible",
    "global.password.good": "Bonne",
    "global.password.strong": "Forte",
    "global.password.tooShort": "Trop court",
    "global.password.tooSimple": "Trop simple"
}