var MESSAGES = {
    "global.ajax.alert.error": "Sorry, an error occurred while processing your request.",
    "global.password.weak": "Weak",
    "global.password.good": "Good",
    "global.password.strong": "Strong",
    "global.password.tooShort": "Too short",
    "global.password.tooSimple": "Too simple"
}