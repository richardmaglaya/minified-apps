//init for session
var http = require('http');
var sessionManager = require('session-manager/session-manager.js');
var sessionManager = sessionManager.create({engine: 'memory'});

var connect = require('connect');
var app = connect()
    .use(connect.bodyParser()) // use this to read body POST requests
    .use(connect.static('../WEB-INF/html'))
    .use(function (req, res) {
        res.setHeader('Content-Type','application/json');

        //get session data
        var session = sessionManager.start(req, res);
        var id = session.get('userID');

        //NO ID, NO ENTRY
        if(id==undefined || id==''){

            //no login functions
            switch(req.url){
                case '/isUserLogged': isUserLogged(req,res); break;
                case '/loginUser': loginUser(req,res); break;
                case '/signUp': rootTitle = 'SignUp'; break;
                case '/verifyCode' : verifyCode(req,res); break;
                case '/resendCode' : resendCode(req,res); break;
                case '/viewtables' : viewTables(req,res); break;

                default: res.end("You are not logged in.");
            }

        }else{

            /*=========================
             PROCESS URL REQUESTS
             ==========================*/
            var rootTitle='';
            switch(req.url){
                //ADD PAGES
                case '/signUp': rootTitle = 'SignUp'; break;
                case '/myAccount': rootTitle = 'MyAccount'; break;
                case '/invoiceRegistry': rootTitle = 'InvoiceRegistry'; break;
                case '/ordersRegistry': rootTitle = 'OrdersRegistry'; break;
                case '/customerRegistry': rootTitle = 'CustomerRegistry'; break;
                case '/supplierRegistry': rootTitle = 'SupplierRegistry'; break;
                case '/employeeRegistry': rootTitle = 'EmployeeRegistry'; break;
                case '/truckerRegistry': rootTitle = 'TruckerRegistry'; break;
                case '/materialsDefinition': rootTitle = 'MaterialsDefinition'; break;
                case '/equipmentsDefinition': rootTitle = 'EquipmentsDefinition'; break;
                case '/taxDefinition': rootTitle = 'TaxDefinition'; break;
                case '/paymentTermDefinition': rootTitle = 'PaymentTermDefinition'; break;
                case '/truckTrailerDefinition': rootTitle = 'TruckTrailerDefinition'; break;
                //ACCESS CHECK
                case '/loginUser': loginUser(req,res);break;
                case '/logoutUser': logoutUser(req,res);break;
                case '/isUserLogged': isUserLogged(req,res);break;
                case '/verifyCode' : verifyCode(req,res); break;
                case '/resendCode' : resendCode(req,res); break;
                case '/getData': getData(req,res);break;
                //UPDATE AND REMOVE
                case '/editData':  updateData(req,res);break;
                case '/removeData':  delData(req,res);break;
                //SEND ORDERS
                case '/sendOrder': emailOrder(req, res); break;
                case '/sendInvoice': emailTheInvoice(req, res); break;
                //STATISTICS
                case '/getStatistics' : getStatistics(req,res); break;

                //TEMP ADMIN
                case '/createTables':  createTables(req,res);break;
                default: res.end("Couldn't find it.");
            }
        }

        //format data for xml and email
        if(rootTitle!=''){
            //SAVE TO DB
            var saveStatus = saveData(req, res, rootTitle);
            //format date
            var today = new Date();
            var logdate = today.getYear()+'.'+today.getMonth()+'.'+today.getDay()+'_'+today.getHours()+'.'+today.getMinutes()+'.'+today.getSeconds();
            rootTitle = rootTitle+' - '+logdate;
            var rootData = req.body;	//send the body posted array
            var mailSubject = rootTitle;
            //GENERATE XML
            //var mailMessage = generateXML(rootTitle, rootData); //generate the xml file
            var mailAttachment = rootTitle+'.xml';
            var mailFrom = "System Notification <rikmaglaya@gmail.com>";
            var mailTo = 'richardmaglaya@gmail.com, ferdinand.borromeo@gmail.com';
            //SEND THE EMAIL WITH THE ATTACHED XML
            //var emailStatus = sendEmail(mailSubject, mailMessage, mailAttachment, mailTo,mailFrom);
        }

    })
    .listen(3000);
console.log('Server is now runnning.');



/*=========================
 VIEW TABLES
 ==========================*/
function viewTables(req,res){
    //reset tables

    var databaseUrl = "mongodb://localhost/mydb"; // "username:password@example.com/mydb"
    var tables = ["users","customers","suppliers","employees","truckers","materials","equipments","trucktrailers","paymentterms","taxdefinition","orders"]
    var db = require("mongojs").connect(databaseUrl, tables);

    res.end('USERS');
    //var data = req.body;
    var check = db.users.find( function(err, users) {
        users.forEach( function(userData) {
            res.end(JSON.stringify(userData));
        });
    });
}


/*=========================
 TIMESTAMP for dateUpdated
 ==========================*/
function myTimeStamp(){
    var today = new Date();
    var myTimeAndDate = today.getYear()+'-'+today.getMonth()+'-'+today.getDay()+' '+today.getHours()+':'+today.getMinutes()+':'+today.getSeconds(); //Y-m-d H:i:s
    return myTimeAndDate;
}

/*=========================
 LOGIN USER
 ==========================*/
function loginUser(req,res){
    var databaseUrl = "mydb";
    // var tables = ["users"]
    var tables = ["users","customers","suppliers","employees","truckers","materials","equipments","trucktrailers","paymentterms","taxdefinition","orders"]
    var db = require("mongojs").connect(databaseUrl, tables);

    var check = db.users.find( {email:req.body.login_email, password:req.body.login_password} , function(err, users) {
        if(users.length>0) {
            users.forEach( function(userData) {
                userData['userID'] = userData.recID;
                res.end(JSON.stringify(userData));
                //assign user info to session
                var session = sessionManager.start(req, res);
                session.set('userID', userData.recID);
                session.set('firstName', userData.firstName);
                session.set('lastName', userData.lastName);
                session.set('email', userData.email);
                session.set('status', userData.status);
                //console.log(userData);
                //log
                logEvent('info', 'user',userData.email+' signed in');
            });
        }else{
            var data = { 'email': ''};
            res.end(JSON.stringify(data));
        }
    });

}

/*=========================
 VERIFY CODE
 ==========================*/
function verifyCode(req,res){
    var databaseUrl = "mydb";
    var tables = ["users"]
    var db = require("mongojs").connect(databaseUrl, tables);
    console.log(req.body);
    var check = db.users.find( { $and: [{ code:req.body.code, email:req.body.email }] } , function(err, users) {
        if(users.length>0) {
            users.forEach( function(userData) {
                res.end(JSON.stringify(userData));
                //update user status
                db.users.update( { email: req.body.email },  { $set:  { status: "1" } }, { multi: true }, function(err, status) {
                    if( err || !status ) { var data =  {'status': 'fail'};
                        //log
                        logEvent('error', 'ERROR_LOG','code verification failed');
                    }
                    else{ var data =  {'status': 'success'}; }

                    res.end(JSON.stringify(data));
                    //log
                    logEvent('info', 'users',req.body.email+' account activated');
                });
                //assign user info to session
                var session = sessionManager.start(req, res);
                session.set('userID', userData.recID);
                session.set('firstName', userData.firstName);
                session.set('lastName', userData.lastName);
                session.set('email', userData.email);
                session.set('status', "1");
                session.set('signUpEmail', '');
            });

        }else{
            var data = { 'status': 'failed'};
            res.end(JSON.stringify(data));
        }
    });

}

/*=========================
 RESEND CODE
 ==========================*/
function resendCode(req,res){
    var databaseUrl = "mydb";
    var tables = ["users"]
    var db = require("mongojs").connect(databaseUrl, tables);

    var check = db.users.find( { email: req.body.signUpEmail }, { code:1, password:1 } , function(err, users) {
        if(users.length>0) {
            console.log(users[0].code);
            //email the code
            var mailSubject = "Welcome to Apps & Pages";
            var mailMessage = "Welcome to Apps & Pages, the easiest way to track transactions online! Please save this";
            mailMessage = mailMessage+ "email as it contains important information about your account.<br/><br/>"
            mailMessage = mailMessage+ "Your Account Details:<br/>";
            mailMessage = mailMessage+ "<br/>Your username is: "+ req.body.signUpEmail;
            mailMessage = mailMessage+ "<br/>Your pasword is: "+ users[0].password;
            mailMessage = mailMessage+ "<br/>Your code is:  "+ users[0].code;
            var mailAttachment = '';
            var mailTo = req.body.signUpEmail;
            var mailFrom = "Apps & Pages <rikmaglaya@gmail.com>";
            sendEmail(mailSubject, mailMessage, mailAttachment, mailTo, mailFrom);

            var data = { 'status': 'success'};
            res.end(JSON.stringify(data));
            //log
            logEvent('info', 'users',req.body.signUpEmail+' code verification resent');
        }else{
            var data = { 'status': 'failed'};
            res.end(JSON.stringify(data));
            //log
            logEvent('error', 'ERROR_LOG',req.body.signUpEmail+' code resent failed');
        }
    });

}

/*=========================
 LOGOUT USER
 ==========================*/
function logoutUser(req,res){
    //clear user info to session
    var session = sessionManager.start(req, res);
    //log
    logEvent('info', 'user',session.get('email')+' signed out');
    session.set('userID', '');
    session.set('firstName', '');
    session.set('lastName', '');
    session.set('email', '');
    session.set('status', '');

    var data = { 'status': 'success'};
    res.end(JSON.stringify(data));
}

/*=========================
 USER SESSION CHECKER
 ==========================*/
function isUserLogged(req,res){
    //clear user info to session
    var session = sessionManager.start(req, res);
    var id = session.get('userID');
    var signUpStatus = session.get('signUpStatus');
    var signUpEmail = session.get('signUpEmail');
    var status = session.get('status');
    if(id==undefined || id=='' || status=='0' ){
        if(signUpStatus=='1'){
            var data = { 'status': 'signup', 'signUpEmail':signUpEmail };
        }else{
            var data = { 'status': 'no'};
        }
    }else{
        var data = {
            'status': 'yes',
            'firstName' : session.get('firstName'),
            'userID' : session.get('userID'),
        };
    }
    res.end(JSON.stringify(data));
}

/*=========================
 EVENT LOGS
 ==========================*/
function logEvent(logType, page, message){
    //format filename
    var d = new Date();
    var year = d.getFullYear();
    var month = d.getMonth(); month++; if(month<10){ month = '0'+month;}
    var day = d.getDate(); if(day<10){ day = '0'+day;}
    var file = page+'_'+year+month+day;
    //write to file
    var fs = require('fs')
        , Log = require('log')
        , log = new Log('logs', fs.createWriteStream("../WEB-INF/logs/"+file+".log", { flags: 'a' }));
    //get message and type
    switch(logType)
    {
        case 'info': log.info(message+'\n'); break;
        case 'debug': log.debug(message+'\n'); break;
        case 'notice': log.notice(message+'\n'); break;
        case 'warning': log.warning(message+'\n'); break;
        case 'error': log.error(message+'\n'); break;
        case 'critical': log.critical(message+'\n'); break;
        case 'alert': log.alert(message+'\n'); break;
        case 'emergency': log.emergency(message+'\n'); break;
    }

}

/*=========================
 GET DATA (These are the the data to be used for display on datatables)
 ==========================*/
function getData(req,res){
    //init session
    var session = sessionManager.start(req, res);
    var userID = session.get('userID');

    var databaseUrl = "mydb"; // "username:password@example.com/mydb"
    var tables = ["users","customers","suppliers","employees","truckers","materials","equipments","trucktrailers","paymentterms","taxdefinition","orders","invoices"]
    var db = require("mongojs").connect(databaseUrl, tables);
    //	db.createCollection(["paymentterms","taxdefinition"]);
    switch(req.body.action){
        case 'getUsers':
            if(req.body.mode=='edit'){
                var rowID = parseInt(req.body.rowID);
                db.users.find({ recID:rowID }, function(err, result) {	res.end(JSON.stringify(result));  });
            }if(req.body.mode=='userData'){
            db.users.find({ recID:userID }, function(err, result) {	res.end(JSON.stringify(result));  });
        }else{
            db.users.find( function(err, data) {	res.end(JSON.stringify(data));});
        }
            break;
        case 'getInvoices':

            switch(req.body.mode){
                case 'getInvoiceNum': //get the last invoice on record by user and increment
                    var doc = db.invoices.find( { userID: userID,  recID: { $gt: 0 } } , { recID:1}, function(err, data) {
                        if(data.length==0){
                            recID = 1;
                        }else{
                            var cnt = data.length - 1; recID = data[cnt].recID + 1;
                        }
                        var invoice_num = generateOrderNo(recID);
                        var data = { 'invoice_num':invoice_num }
                        res.end(JSON.stringify(data));
                    });
                    break;
                case 'all':
                    db.invoices.find( {userID:userID}, function(err, data) {	res.end(JSON.stringify(data));});
                    break;
                case 'draft':
                    db.invoices.find( {userID:userID, status:'draft'}, function(err, data) {	res.end(JSON.stringify(data));});
                    break;
                case 'sent':
                    db.invoices.find( {userID:userID, status:'sent'}, function(err, data) {	res.end(JSON.stringify(data));});
                    break;
                case 'paid':
                    db.invoices.find( {userID:userID, status:'paid'}, function(err, data) {	res.end(JSON.stringify(data));});
                    break;
                case 'view':
                    var rowID = parseInt(req.body.rowID);
                    db.invoices.find( {userID:userID, recID:rowID}, function(err, data) {	res.end(JSON.stringify(data));});
                    break;

                default: db.invoices.find( {userID:userID}, function(err, data) {	res.end(JSON.stringify(data));});
            }

            break;
        case 'getOrders':
            if(req.body.mode=='edit' || req.body.mode=='view'){
                var rowID = parseInt(req.body.rowID);
                db.orders.find({ recID:rowID, userID:userID}, function(err, result) {	res.end(JSON.stringify(result));  });
            }else if(req.body.mode=='invoice_status'){
                var invoiceStatus = req.body.invoiceStatus;
                db.orders.find( {userID:userID, invoiceStatus:invoiceStatus}, function(err, data) {	res.end(JSON.stringify(data));});
            }else{
                db.orders.find( {userID:userID}, function(err, data) {	res.end(JSON.stringify(data));});
            }
            break;
        case 'getCustomers':
            if(req.body.mode=='edit'){
                var rowID = parseInt(req.body.rowID);
                db.customers.find({ recID:rowID, userID:userID }, function(err, result) {	res.end(JSON.stringify(result));  });
            }else{
                db.customers.find( {userID:userID},  function(err, data) {	res.end(JSON.stringify(data));});
            }
            break;
        case 'getSuppliers':
            if(req.body.mode=='edit'){
                var rowID = parseInt(req.body.rowID);
                db.suppliers.find({ recID:rowID, userID:userID }, function(err, result) {	res.end(JSON.stringify(result));  });
            }else{
                db.suppliers.find( {userID:userID},  function(err, data) {	res.end(JSON.stringify(data));});
            }
            break;
        case 'getEmployees':
            if(req.body.mode=='edit'){
                var rowID = parseInt(req.body.rowID);
                db.employees.find({ recID:rowID, userID:userID }, function(err, result) {	res.end(JSON.stringify(result));  });
            }else{
                db.employees.find( {userID:userID},  function(err, data) {	res.end(JSON.stringify(data));});
            }
            break;
        case 'getTruckers':
            if(req.body.mode=='edit'){
                var rowID = parseInt(req.body.rowID);
                db.truckers.find({ recID:rowID, userID:userID }, function(err, result) {	res.end(JSON.stringify(result));  });
            }else{
                db.truckers.find( {userID:userID},  function(err, data) {	res.end(JSON.stringify(data));});
            }
            break;
        case 'getMaterials':
            if(req.body.mode=='edit'){
                var rowID = parseInt(req.body.rowID);
                db.materials.find({ recID:rowID, userID:userID }, function(err, result) {	res.end(JSON.stringify(result));  });
            }else{
                db.materials.find( {userID:userID},  function(err, data) {	res.end(JSON.stringify(data));});
            }
            break;
        case 'getEquipments':
            if(req.body.mode=='edit'){
                var rowID = parseInt(req.body.rowID);
                db.equipments.find({ recID:rowID, userID:userID }, function(err, result) {	res.end(JSON.stringify(result));  });
            }else{
                db.equipments.find( {userID:userID},  function(err, data) {	res.end(JSON.stringify(data));});
            }
            break;
        case 'getTruckTrailers':
            if(req.body.mode=='edit'){
                var rowID = parseInt(req.body.rowID);
                db.trucktrailers.find({ recID:rowID, userID:userID }, function(err, result) {	res.end(JSON.stringify(result));  });

            }else{
                db.trucktrailers.find( {userID:userID},  function(err, data) {	res.end(JSON.stringify(data));});
            }
            break;
        case 'getPaymentTerms':
            if(req.body.mode=='edit'){
                var rowID = parseInt(req.body.rowID);
                db.paymentterms.find({ recID:rowID, userID:userID }, function(err, result) {	res.end(JSON.stringify(result));  });

            }else{
                db.paymentterms.find( {userID:userID},  function(err, data) {	res.end(JSON.stringify(data));});
            }
            break;
        case 'getTaxDefinition':
            if(req.body.mode=='edit'){
                var rowID = parseInt(req.body.rowID);
                db.taxdefinition.find({ recID:rowID, userID:userID }, function(err, result) {	res.end(JSON.stringify(result));  });

            }else{
                db.taxdefinition.find( {userID:userID},  function(err, data) {	res.end(JSON.stringify(data));});
            }
            break;
    }

    //log
    logEvent('info', 'page',req.body.action+' was requested');
}
/*=========================
 SAVE DATA (These are the the data submitted via forms)
 ==========================*/

function saveData(req, res, rootTitle){

    var databaseUrl = "mydb";
    var tables = ["users","customers","suppliers","employees","truckers","materials","equipments","trucktrailers","paymentterms","taxdefinition","orders","invoices"]
    var db = require("mongojs").connect(databaseUrl, tables);

    if(rootTitle!='SignUp'){
        //init session
        var session = sessionManager.start(req, res);
        var userID = session.get('userID');
        //add userID field on each table upon entry
        req.body['userID'] = userID;
        //add dateCreated
        req.body['dateCreated'] = myTimeStamp();
    }

    switch(rootTitle){
        case 'SignUp':
            //db.users.find( { $and: [{firstName: req.body.firstName}, { lastName: req.body.lastName }, { email: req.body.email }] } , function(err, users) {
            db.users.find( { email: req.body.email } , function(err, users) {
                if(users.length>0) {var data =  { 'status': 'exist'};
                    res.end('email address is already in use.');
                    console.log('email in use');
                    //log
                    //logEvent('warning', 'users',req.body+' duplicate entry or already exist');
                }else{
                    //additional fields
                    var theCode = generateCode();	console.log(theCode);
                    req.body['code'] = theCode;
                    req.body['status'] = '0';	//default must be 0
                    req.body['dateCreated'] = myTimeStamp();

                    //get last id
                    var recID = 0;
                    var doc = db.users.find( { recID: { $gt: 0 } } , { recID:1}, function(err, result) {
                        if(result.length==0){
                            req.body['recID'] = 1;
                        }else{
                            var cnt = result.length - 1;
                            req.body['recID'] = result[cnt].recID + 1;
                        }

                        if(req.files.file.name.length>4)
                        {
                            //file upload starts here
                            var imageLogo = '';
                            req.body['logo'] = '';

                            var path = require('path'),
                                fs = require('fs.extra');
                            imageLogo = req.body['recID']+'.png';
                            var tempPath = req.files.file.path,
                                targetPath = path.resolve('../WEB-INF/html/images/uploads/'+imageLogo);
                            //remove file if already exist
                            fs.exists(targetPath, function(exists) {
                                if (exists) {
                                    fs.unlink(targetPath, function(err) {
                                        if (err){ throw err;
                                            //log
                                            logEvent('error', 'ERROR_LOG',targetPath+' file wa not deleted/cleared');
                                        }else{
                                            console.log(req.files.file.name+"Upload successful!");
                                            //log
                                            logEvent('info', 'users',targetPath+' File upload successful.');
                                        }
                                    });
                                }
                            });

                            fs.move(tempPath, targetPath, function(err) {
                                if (err) {
                                    throw err;
                                    //log
                                    logEvent('error', 'ERROR_LOG', targetPath+' file rename failed');
                                }else{
                                    console.log(req.files.file.name+"Upload successful!");
                                    //log
                                    logEvent('info', 'users', targetPath+' file renamed successful!');
                                }
                            });
                            //file upload ends here
                            req.body['logo'] = imageLogo;
                        }

                        //insert data to database
                        db.users.insert([req.body], function(err, saved) {
                            if( err || !saved ) {
                                var data =  { 'status': 'data not saved'};
                                //log
                                logEvent('error', 'ERROR_LOG',' signup failed or data was not inserted to the db');
                            }
                            else{
                                var data =  { 'status': 'success'};
                                //email the code
                                var mailSubject = "Welcome to Apps & Pages";
                                var mailMessage = "Welcome to Apps & Pages, the easiest way to track transactions online! Please save this";
                                mailMessage = mailMessage+ "email as it contains important information about your account.<br/><br/>"
                                mailMessage = mailMessage+ "Your Account Details:<br/>";
                                mailMessage = mailMessage+ "<br/>Your username is: "+ req.body.email;
                                mailMessage = mailMessage+ "<br/>Your pasword is: "+ req.body.password;
                                mailMessage = mailMessage+ "<br/>Your code is:  "+ theCode;
                                var mailAttachment = '';
                                var mailTo = req.body.email;
                                var mailFrom = "Apps & Pages <rikmaglaya@gmail.com>";

                                //sendEmail(mailSubject, mailMessage, mailAttachment, mailTo, mailFrom);

                                //assign to session the status after sign up
                                var session = sessionManager.start(req, res);
                                session.set('signUpStatus', '1');
                                session.set('signUpEmail', req.body.email);
                            } res.end(JSON.stringify(data));
                        });

                    } ).sort( { recID: -1 } ).limit(1);
                    var data =  {'status': 'success'};

                    //log
                    logEvent('info', 'sign_up',req.body.email+' signed up');
                } res.end(JSON.stringify(data));
            });

            console.log('redirect to index, then trucking to force refresh');
            //	redirect to main page
            res.writeHead(301, {Location: 'http://ec2-107-20-118-74.compute-1.amazonaws.com:3000/index.html'});
            res.end();

            break;
        case 'InvoiceRegistry':
            db.invoices.find( { $and: [ {userID: userID}, {invoiceNum: req.body.invoiceNum}, { customerID: req.body.customerID }] } , function(err, users) {
                if(users.length>0) {var data =  { 'status': 'exist'};
                    res.end(JSON.stringify(data));
                    //log
                    logEvent('info', 'invoices',req.body.invoiceNum+' invoice created');
                }else{
                    //get last id
                    var recID = 0;
                    var doc = db.invoices.find( { userID:userID, recID: { $gt: 0 } } , { recID:1}, function(err, data) {
                        if(data.length==0){
                            recID = 1;
                            req.body['recID'] = recID;
                            //insert data to database
                            db.invoices.insert([req.body], function(err, saved) {
                                if( err || !saved ) { var data =  { 'status': 'data not saved'};
                                    //log
                                    logEvent('error', 'ERROR_LOG','invoice not saved');
                                }
                                else{ var data =  { 'status': 'success'};} res.end(JSON.stringify(data));
                            });

                        }else{
                            var cnt = data.length - 1; recID = data[cnt].recID + 1;
                            req.body['recID'] = recID;
                            //insert data to database
                            db.invoices.insert([req.body], function(err, status) {
                                if( err || !status ) { var data =  {'status': 'fail'};
                                    //log
                                    logEvent('error', 'ERROR_LOG','invoice not saved');
                                }
                                else{ var data =  {'status': 'success'}; }
                            });
                        }

                        //check if mode is save or save and send an email to customer
                        if(req.body.mode=='sendInvoice'){
                            req.body.rowID = recID;
                            emailTheInvoice(req,res);
                        }

                    } ).sort( { recID: -1 } ).limit(1);


                    var data =  {'status': 'success'};
                    //log
                    logEvent('info', 'invoices','invoice : '+req.body.invoiceNum+' - Date: '+req.body.invoiceDate+' created');
                } res.end(JSON.stringify(data));
            });


            break;
        case 'OrdersRegistry':
            db.orders.find( { $and: [ {userID: userID}, {customer: req.body.customer}, { supplier: req.body.supplier }, { transactionDate: req.body.transactionDate }] } , function(err, users) {
                if(users.length>0) {var data =  { 'status': 'exist'};
                    //log
                    logEvent('info', 'users',req.body.signUpEmail+' code verification resent');
                }
                else{
                    //get last id
                    var recID = 0;
                    var doc = db.orders.find( { recID: { $gt: 0 } } , { recID:1}, function(err, data) {
                        if(data.length==0){
                            recID = 1;
                            req.body['recID'] = recID;
                            req.body['orderNo'] = generateOrderNo(recID);
                            //insert data to database
                            db.orders.insert([req.body], function(err, saved) {
                                if( err || !saved ) { var data =  { 'status': 'data not saved'};}
                                else{ var data =  { 'status': 'success'};} res.end(JSON.stringify(data));
                            });

                        }else{
                            var cnt = data.length - 1; recID = data[cnt].recID + 1;
                            req.body['recID'] = recID;
                            req.body['orderNo'] = generateOrderNo(recID);

                            //insert data to database
                            db.orders.insert([req.body], function(err, status) {
                                if( err || !status ) { var data =  {'status': 'fail'};
                                    //log
                                    logEvent('error', 'ERROR_LOG','order not saved');
                                }
                                else{ var data =  {'status': 'success'}; }
                            });
                        }

                        //check if mode is save or save and send an email to customer
                        if(req.body.orderMode=='save_and_send'){
                            req.body.rowID = recID;
                            emailOrder(req,res);
                        }

                    } ).sort( { recID: -1 } ).limit(1);


                    var data =  {'status': 'success'};
                    //log
                    logEvent('info', 'orders','Customer: '+req.body.customer+' - Supplier: '+req.body.supplier+' - Date: '+req.body.transactionDate+' registered');
                } res.end(JSON.stringify(data));
            });
            break;
        case 'CustomerRegistry':
            db.customers.find( { $and: [ {userID: userID}, {cust_num: req.body.cust_num}, { cust_name: req.body.cust_name }, { email_add: req.body.email_add }] } , function(err, users) {
                if(users.length>0) {var data =  { 'status': 'exist'};	}
                else{
                    //get last id
                    var recID = 0;
                    var doc = db.customers.find( { recID: { $gt: 0 } } , { recID:1}, function(err, data) {
                        if(data.length==0){
                            req.body['recID'] = 1;
                            //insert data to database
                            db.customers.insert([req.body], function(err, saved) {
                                if( err || !saved ) { var data =  { 'status': 'data not saved'};}
                                else{ var data =  { 'status': 'success'};} res.end(JSON.stringify(data));
                            });
                        }else{
                            var cnt = data.length - 1;
                            req.body['recID'] = data[cnt].recID + 1;
                            //insert data to database
                            db.customers.insert([req.body], function(err, status) {
                                if( err || !status ) { var data =  {'status': 'fail'};
                                    //log
                                    logEvent('error', 'ERROR_LOG','customer not saved');
                                }
                                else{ var data =  {'status': 'success'}; }
                            });
                        }
                    } ).sort( { recID: -1 } ).limit(1);
                    var data =  {'status': 'success'};
                    //log
                    logEvent('info', 'customers',req.body.email_add+' registered');
                } res.end(JSON.stringify(data));
            });
            break;
        case 'SupplierRegistry':
            db.suppliers.find( { $and: [ {userID: userID}, {cust_num: req.body.cust_num}, { cust_name: req.body.cust_name }, { email_add: req.body.email_add }] } , function(err, users) {
                if(users.length>0) {var data =  { 'status': 'exist'};	}
                else{
                    //get last id
                    var recID = 0;
                    var doc = db.suppliers.find( { recID: { $gt: 0 } } , { recID:1}, function(err, data) {
                        if(data.length==0){
                            req.body['recID'] = 1;
                            //insert data to database
                            db.suppliers.insert([req.body], function(err, saved) {
                                if( err || !saved ) { var data =  { 'status': 'data not saved'};
                                    //log
                                    logEvent('error', 'ERROR_LOG','supplier not saved');
                                }
                                else{ var data =  { 'status': 'success'};} res.end(JSON.stringify(data));
                            });
                        }else{
                            var cnt = data.length - 1;
                            req.body['recID'] = data[cnt].recID + 1;
                            //insert data to database
                            db.suppliers.insert([req.body], function(err, saved) {
                                if( err || !saved ) { var data =  { 'status': 'data not saved'};}
                                else{ var data =  { 'status': 'success'};} res.end(JSON.stringify(data));
                            });
                        }
                    } ).sort( { recID: -1 } ).limit(1);
                    var data =  {'status': 'success'};
                    //log
                    logEvent('info', 'suppliers',req.body.email_add+' added');
                } res.end(JSON.stringify(data));
            });
            break;
        case 'EmployeeRegistry':
            db.employees.find( { $and: [ {userID: userID}, {cust_num: req.body.cust_num}, { cust_name: req.body.cust_name }, { email_add: req.body.email_add }] } , function(err, users) {
                if(users.length>0) {var data =  { 'status': 'exist'};	}
                else{
                    //get last id
                    var recID = 0;
                    var doc = db.employees.find( { recID: { $gt: 0 } } , { recID:1}, function(err, data) {
                        if(data.length==0){
                            req.body['recID'] = 1;
                            //insert data to database
                            db.employees.insert([req.body], function(err, saved) {
                                if( err || !saved ) { var data =  { 'status': 'data not saved'};
                                    //log
                                    logEvent('error', 'ERROR_LOG','employee not created');
                                }
                                else{ var data =  { 'status': 'success'};} res.end(JSON.stringify(data));
                            });
                        }else{
                            var cnt = data.length - 1;
                            req.body['recID'] = data[cnt].recID + 1;
                            //insert data to database
                            db.employees.insert([req.body], function(err, saved) {
                                if( err || !saved ) { var data =  { 'status': 'data not saved'};
                                    //log
                                    logEvent('error', 'ERROR_LOG','employee not created');
                                }
                                else{ var data =  { 'status': 'success'};} res.end(JSON.stringify(data));
                            });
                        }
                    } ).sort( { recID: -1 } ).limit(1);
                    var data =  {'status': 'success'};
                    //log
                    logEvent('info', 'employees',req.body.email_add+' added');
                } res.end(JSON.stringify(data));
            });
            break;
        case 'TruckerRegistry':
            db.truckers.find( { $and: [ {userID: userID}, {cust_num: req.body.cust_num}, { cust_name: req.body.cust_name }, { email_add: req.body.email_add }] } , function(err, users) {
                if(users.length>0) {var data =  { 'status': 'exist'};	}
                else{
                    //get last id
                    var recID = 0;
                    var doc = db.truckers.find( { recID: { $gt: 0 } } , { recID:1}, function(err, data) {
                        if(data.length==0){
                            req.body['recID'] = 1;
                            //insert data to database
                            db.truckers.insert([req.body], function(err, saved) {
                                if( err || !saved ) { var data =  { 'status': 'data not saved'};
                                    //log
                                    logEvent('error', 'ERROR_LOG','trucker not saved');
                                }
                                else{ var data =  { 'status': 'success'};} res.end(JSON.stringify(data));
                            });
                        }else{
                            var cnt = data.length - 1;
                            req.body['recID'] = data[cnt].recID + 1;
                            //insert data to database
                            db.truckers.insert([req.body], function(err, saved) {
                                if( err || !saved ) { var data =  { 'status': 'data not saved'};
                                    //log
                                    logEvent('error', 'ERROR_LOG','trucker not saved');
                                }
                                else{ var data =  { 'status': 'success'};} res.end(JSON.stringify(data));
                            });
                        }
                    } ).sort( { recID: -1 } ).limit(1);
                    var data =  {'status': 'success'};
                    //log
                    logEvent('info', 'trucker',req.body.email_add+' added');
                } res.end(JSON.stringify(data));
            });
            break;
        case 'MaterialsDefinition':
            db.materials.find({ $and: [ {userID: userID}, {material_num: req.body.material_num}, { material_name: req.body.material_name }] } , function(err, users) {
                if(users.length>0) {var data =  { 'status': 'exist'};	}
                else{
                    //get last id
                    var recID = 0;
                    var doc = db.materials.find( { recID: { $gt: 0 } } , { recID:1}, function(err, data) {
                        if(data.length==0){
                            req.body['recID'] = 1;
                            //insert data to database
                            db.materials.insert([req.body], function(err, saved) {
                                if( err || !saved ) { var data =  { 'status': 'data not saved'};
                                    //log
                                    logEvent('error', 'ERROR_LOG','materials not saved');
                                }
                                else{ var data =  { 'status': 'success'};} res.end(JSON.stringify(data));
                            });
                        }else{
                            var cnt = data.length - 1;
                            req.body['recID'] = data[cnt].recID + 1;
                            //insert data to database
                            db.materials.insert([req.body], function(err, saved) {
                                if( err || !saved ) { var data =  [{ 'status': 'data not saved'}];
                                    //log
                                    logEvent('error', 'ERROR_LOG','material not saved');
                                }
                                else{ var data =  { 'status': 'success'};} res.end(JSON.stringify(data));
                            });
                        }
                    } ).sort( { recID: -1 } ).limit(1);
                    var data =  {'status': 'success'};
                    //log
                    logEvent('info', 'materials',req.body.material_num+' '+req.body.material_name+' added');
                } res.end(JSON.stringify(data));

            });
            break;
        case 'EquipmentsDefinition':
            db.equipments.find({ $and: [ {userID: userID}, {equipment_num: req.body.equipment_num}, { equipment_name: req.body.equipment_name }] } , function(err, users) {
                if(users.length>0) {var data =  { 'status': 'exist'};	}
                else{
                    //get last id
                    var recID = 0;
                    var doc = db.equipments.find( { recID: { $gt: 0 } } , { recID:1}, function(err, data) {
                        if(data.length==0){
                            req.body['recID'] = 1;
                            //insert data to database
                            db.equipments.insert([req.body], function(err, saved) {
                                if( err || !saved ) { var data =  { 'status': 'data not saved'};
                                    //log
                                    logEvent('error', 'ERROR_LOG','equipment not saved');
                                }
                                else{ var data =  { 'status': 'success'};} res.end(JSON.stringify(data));
                            });
                        }else{
                            var cnt = data.length - 1;
                            req.body['recID'] = data[cnt].recID + 1;
                            //insert data to database
                            db.equipments.insert([req.body], function(err, saved) {
                                if( err || !saved ) { var data =  [{ 'status': 'data not saved'}];
                                    //log
                                    logEvent('error', 'ERROR_LOG','equipment not saved');
                                }
                                else{ var data =  { 'status': 'success'};} res.end(JSON.stringify(data));
                            });
                        }
                    } ).sort( { recID: -1 } ).limit(1);
                    var data =  {'status': 'success'};
                    //log
                    logEvent('info', 'equipments',req.body.equipment_num+' '+req.body.equipment_name+' added');
                } res.end(JSON.stringify(data));

            });
            break;
        case 'TaxDefinition': //var table = db.taxes;
            db.taxdefinition.find( { $and: [ {userID: userID}, {federal_tax: req.body.federal_tax}, { local_tax: req.body.local_tax }, { union_dues: req.body.union_dues }, { dispatch_fee: req.body.dispatch_fee }] } , function(err, users) {
                if(users.length>0) {var data =  { 'status': 'exist'};	}
                else{
                    //get last id
                    var recID = 0;
                    var doc = db.taxdefinition.find( { recID: { $gt: 0 } } , { recID:1}, function(err, data) {
                        if(data.length==0){
                            req.body['recID'] = 1;
                            //insert data to database
                            db.taxdefinition.insert([req.body], function(err, saved) {
                                if( err || !saved ) { var data =  { 'status': 'data not saved'};
                                    //log
                                    logEvent('error', 'ERROR_LOG','tax not saved');
                                }
                                else{ var data =  { 'status': 'success'};} res.end(JSON.stringify(data));
                            });
                        }else{
                            var cnt = data.length - 1;
                            req.body['recID'] = data[cnt].recID + 1;
                            //insert data to database
                            db.taxdefinition.insert([req.body], function(err, saved) {
                                if( err || !saved ) { var data =  { 'status': 'data not saved'};
                                    //log
                                    logEvent('error', 'ERROR_LOG','tax not saved');
                                }
                                else{ var data =  { 'status': 'success'};} res.end(JSON.stringify(data));
                            });
                        }
                    } ).sort( { recID: -1 } ).limit(1);
                    var data =  {'status': 'success'};
                    //log
                    logEvent('info', 'taxdefinition',req.body.federal_tax+' '+req.body.local_tax+' new tax defined');
                } res.end(JSON.stringify(data));
            });
            break;
        case 'PaymentTermDefinition': //var table = db.terms;
            db.paymentterms.find( { $and: [ {userID: userID}, {terms_num: req.body.terms_num}, { terms_name: req.body.terms_name }] } , function(err, users) {
                if(users.length>0) {var data =  { 'status': 'exist'};	}
                else{
                    //get last id
                    var recID = 0;
                    var doc = db.paymentterms.find( { recID: { $gt: 0 } } , { recID:1}, function(err, data) {
                        if(data.length==0){
                            req.body['recID'] = 1;
                            //insert data to database
                            db.paymentterms.insert([req.body], function(err, saved) {
                                if( err || !saved ) { var data =  { 'status': 'data not saved'};
                                    //log
                                    logEvent('error', 'ERROR_LOG','payment term not saved');
                                }
                                else{ var data =  { 'status': 'success'};} res.end(JSON.stringify(data));
                            });
                        }else{
                            var cnt = data.length - 1;
                            req.body['recID'] = data[cnt].recID + 1;
                            //insert data to database
                            db.paymentterms.insert([req.body], function(err, saved) {
                                if( err || !saved ) { var data =  { 'status': 'data not saved'};
                                    //log
                                    logEvent('error', 'ERROR_LOG','payment term not saved');
                                }
                                else{ var data =  { 'status': 'success'};} res.end(JSON.stringify(data));
                            });
                        }
                    } ).sort( { recID: -1 } ).limit(1);
                    var data =  {'status': 'success'};
                    //log
                    logEvent('info', 'paymentterms',req.body.terms_num+'-'+req.body.terms_name+' paymentterms defined');
                } res.end(JSON.stringify(data));
            });
            break;
        case 'TruckTrailerDefinition':
            db.trucktrailers.find( { $and: [ {userID: userID}, {truck_num: req.body.truck_num}, { year_and_make: req.body.year_and_make }, { vin: req.body.vin }] } , function(err, users) {
                if(users.length>0) {var data =  { 'status': 'exist'}; 	}
                else{
                    //get last id
                    var recID = 0;
                    var doc = db.trucktrailers.find( { recID: { $gt: 0 } } , { recID:1}, function(err, data) {
                        if(data.length==0){
                            req.body['recID'] = 1;
                            //insert data to database
                            db.trucktrailers.insert([req.body], function(err, saved) {
                                if( err || !saved ) { var data =  { 'status': 'data not saved'};
                                    //log
                                    logEvent('error', 'ERROR_LOG','truck trailer not saved');
                                }
                                else{ var data =  { 'status': 'success'};} res.end(JSON.stringify(data));
                            });
                        } else{
                            var cnt = data.length - 1;
                            req.body['recID'] = data[cnt].recID + 1;
                            //insert data to database
                            db.trucktrailers.insert([req.body], function(err, saved) {
                                if( err || !saved ) { var data =  { 'status': 'data not saved'};
                                    //log
                                    logEvent('error', 'ERROR_LOG','truck trailer not saved');
                                }
                                else{ var data =  { 'status': 'success'};} res.end(JSON.stringify(data));
                            });
                        }
                    } ).sort( { recID: -1 } ).limit(1);
                    var data =  {'status': 'success'};
                    //log
                    logEvent('info', 'truck_trailer',req.body.truck_num+'-'+req.body.year_and_make+'-'+req.body.vin+' truck trailer added');
                } res.end(JSON.stringify(data));
            });
            break;
        default:
    }
}

/*=========================
 UPDATE DATA (These are the the data submitted via pop-up forms on click of the EDIT buttons on datatables)
 ==========================*/

function updateData(req, res){
    //init session
    var session = sessionManager.start(req, res);
    var userID = session.get('userID');

    var databaseUrl = "mydb"; // "username:password@example.com/mydb"
    var tables = ["users","customers","suppliers","employees","truckers","materials","equipments","trucktrailers","paymentterms","taxdefinition","orders","invoices"]
    var db = require("mongojs").connect(databaseUrl, tables);

    var rowID = parseInt(req.body.rowID);
    delete req.body.rowID;	 //exclude field for update

    req.body['dateUpdated'] = myTimeStamp(); // add timestamp on update

    switch(req.body.action){
        case 'editMyAccount':

            //check if file was uploaded
            if(req.files.file.name.length>4)
            {
                var imageLogo = '';
                req.body['logo'] = '';
                var path = require('path'),
                    fs = require('fs.extra');
                imageLogo = req.body.userID+'.png';
                var tempPath = req.files.file.path,
                    targetPath = path.resolve('../WEB-INF/html/images/uploads/'+imageLogo);


                //remove file if already exist
                fs.exists(targetPath, function(exists) {
                    if (exists) {
                        fs.unlink(targetPath, function(err) {
                            if (err) { throw err;
                                //log
                                logEvent('error', 'ERROR_LOG','image was not  cleared');
                            }else{
                                console.log(req.files.file.name+"Upload successful!");
                            }
                        });
                    }
                });

                fs.move(tempPath, targetPath, function(err) {
                    if (err) {throw err;
                        //log
                        logEvent('error', 'ERROR_LOG','image not renamed');
                    }else{
                        console.log(req.files.file.name+"Upload successful!");
                    }
                });
                //file upload ends here
                req.body['logo'] = imageLogo;
            }
            console.log(req.body);
            //update db
            db.users.update( { recID: parseInt(req.body.userID) },  { $set:  req.body }, { multi: true }, function(err, status) {
                if( err || !status ) { var data =  {'status': 'fail'};
                    //log
                    logEvent('error', 'ERROR_LOG','user data failed to update');
                }
                else{ var data =  {'status': 'success'}; }
                res.end(JSON.stringify(data));
            });

            //assign userid and the updated firstname to session
            var session = sessionManager.start(req, res);
            session.set('firstName', req.body.firstName);
            session.set('userID', req.body.userID);

            //redirect to main page
            res.writeHead(301, {Location: 'http://localhost:3000/index.html'});
            res.end();

            break;
        case 'editInvoices':
            db.invoices.update( { recID: rowID },  { $set:  req.body }, { multi: true }, function(err, status) {
                if( err || !status ) { var data =  {'status': 'fail'};
                    //log
                    logEvent('error', 'ERROR_LOG','unable to update invoices ');
                }
                else{ var data =  {'status': 'success'}; }
                res.end(JSON.stringify(data));
                //check if mode is save or save and send an email to customer
                if(req.body.orderMode=='save_and_send'){
                    req.body.rowID = rowID;
                    //emailTheInvoice(req, res);
                }
                //log
                logEvent('info', 'orders','Invoice ID.: '+req.body.rowID+' updated');
            });
            break;
        case 'editOrders':
            db.orders.update( { recID: rowID },  { $set:  req.body }, { multi: true }, function(err, status) {
                if( err || !status ) { var data =  {'status': 'fail'};
                    //log
                    logEvent('error', 'ERROR_LOG','unable to update order ');
                }
                else{ var data =  {'status': 'success'}; }
                res.end(JSON.stringify(data));
                //check if mode is save or save and send an email to customer
                if(req.body.orderMode=='sendInvoice'){
                    req.body.rowID = rowID;
                    emailTheInvoice(req, res);
                }
                //log
                logEvent('info', 'invoices','Invoice No.: '+req.body.rowID+' updated');
            });
            break;
        case 'editCustomers':
            db.customers.update( { recID: rowID },  { $set:  req.body }, { multi: true }, function(err, status) {
                if( err || !status ) { var data =  {'status': 'fail'};
                    //log
                    logEvent('error', 'ERROR_LOG','unable to update customer ');
                }
                else{ var data =  {'status': 'success'}; }
                res.end(JSON.stringify(data));
                //log
                logEvent('info', 'customers',req.body.email_add+' updated');
            });
            break;
        case 'editSuppliers':
            db.suppliers.update( { recID: rowID },  { $set:  req.body }, { multi: true }, function(err, status) {
                if( err || !status ) { var data =  {'status': 'fail'};
                    //log
                    logEvent('error', 'ERROR_LOG','unable to update supplier ');
                }
                else{ var data =  {'status': 'success'}; }
                res.end(JSON.stringify(data));
                //log
                logEvent('info', 'suppliers',req.body.email_add+' updated');
            });
            break;
        case 'editEmployees':
            db.employees.update( { recID: rowID },  { $set:  req.body }, { multi: true }, function(err, status) {
                if( err || !status ) { var data =  {'status': 'fail'};
                    //log
                    logEvent('error', 'ERROR_LOG','unable to update employee ');
                }
                else{ var data =  {'status': 'success'}; }
                res.end(JSON.stringify(data));
                //log
                logEvent('info', 'employees',req.body.email_add+' updated');
            });
            break;
        case 'editTruckers':
            db.truckers.update( { recID: rowID },  { $set:  req.body }, { multi: true }, function(err, status) {
                if( err || !status ) { var data =  {'status': 'fail'};
                    //log
                    logEvent('error', 'ERROR_LOG','unable to update trucker ');
                }
                else{ var data =  {'status': 'success'}; }
                res.end(JSON.stringify(data));
                //log
                logEvent('info', 'truckers',req.body.email_add+' updated');
            });
            break;
        case 'editMaterials':
            db.materials.update( { recID: rowID },  { $set:  req.body }, { multi: true }, function(err, status) {
                if( err || !status ) { var data =  {'status': 'fail'};
                    //log
                    logEvent('error', 'ERROR_LOG','unable to update material ');
                }
                else{ var data =  {'status': 'success'}; }
                res.end(JSON.stringify(data));
                //log
                logEvent('info', 'materials',req.body.material_num+' '+req.body.material_name+' updated');
            });
            break;
        case 'editEquipments':
            db.equipments.update( { recID: rowID },  { $set:  req.body }, { multi: true }, function(err, status) {
                if( err || !status ) { var data =  {'status': 'fail'};
                    //log
                    logEvent('error', 'ERROR_LOG','unable to update equipment ');
                }
                else{ var data =  {'status': 'success'}; }
                res.end(JSON.stringify(data));
                //log
                logEvent('info', 'equipments',req.body.equipment_num+' '+req.body.equipment_name+' updated');
            });
            break;
        case 'editTruckTrailers':
            db.trucktrailers.update( { recID: rowID },  { $set:  req.body }, { multi: true }, function(err, status) {
                if( err || !status ) { var data =  {'status': 'fail'};
                    //log
                    logEvent('error', 'ERROR_LOG','unable to update truck trailers ');
                }
                else{ var data =  {'status': 'success'}; }
                res.end(JSON.stringify(data));
                //log
                logEvent('info', 'truck_trailers',req.body.truck_num+' '+req.body.vin+' updated');
            });
            break;
        case 'editPaymentTerms':
            db.paymentterms.update( { recID: rowID },  { $set:  req.body }, { multi: true }, function(err, status) {
                if( err || !status ) { var data =  {'status': 'fail'};
                    //log
                    logEvent('error', 'ERROR_LOG','unable to update payment term ');
                }
                else{ var data =  {'status': 'success'}; }
                res.end(JSON.stringify(data));
                //log
                logEvent('info', 'paymentterms',req.body.truck_num+' '+req.body.vin+' updated');
            });
            break;
        case 'editTaxDefinition':
            db.taxdefinition.update( { recID: rowID },  { $set:  req.body }, { multi: true }, function(err, status) {
                if( err || !status ) { var data =  {'status': 'fail'};
                    //log
                    logEvent('error', 'ERROR_LOG','unable to update tax definition ');
                }
                else{ var data =  {'status': 'success'}; }
                res.end(JSON.stringify(data));
                //log
                logEvent('info', 'taxdefinition',req.body.truck_num+' '+req.body.vin+' updated');
            });
            break;
        default:
    }
}

/*=========================
 DELETE DATA (These are the the data to be deleted when you click the DELETE button via datatables)
 ==========================*/

function delData(req,res){
    var databaseUrl = "mydb"; // "username:password@example.com/mydb"
    var tables = ["users","customers","suppliers","employees","truckers","materials","equipments","trucktrailers","paymentterms","taxdefinition","orders","invoices"]
    var db = require("mongojs").connect(databaseUrl, tables);

    var rowID = parseInt(req.body.rowID);

    switch(req.body.action){
        // case 'delUsers': db.users.remove( { recID:rowID } ); break;
        case 'delInvoices':db.invoices.remove( { recID:rowID }, function(err, status) {
            if( err || !status ) { var data =  { 'status': 'fail'};
                //log
                logEvent('error', 'ERROR_LOG','unable to delete invoice ');
            }
            else{ var data =  { 'status': 'success'};} res.end(JSON.stringify(data));
        });
            //log
            logEvent('info', 'orders','Invoice recID: '+rowID+' deleted');
            break;
        case 'delOrders':db.orders.remove( { recID:rowID }, function(err, status) {
            if( err || !status ) { var data =  { 'status': 'fail'};
                //log
                logEvent('error', 'ERROR_LOG','unable to delete order ');
            }
            else{ var data =  { 'status': 'success'};} res.end(JSON.stringify(data));
        });
            //log
            logEvent('info', 'orders','Order No.: '+rowID+' deleted');
            break;
        case 'delCustomers':db.customers.remove( { recID:rowID }, function(err, status) {
            if( err || !status ) { var data =  { 'status': 'fail'};
                //log
                logEvent('error', 'ERROR_LOG','unable to delete customer ');
            }
            else{ var data =  { 'status': 'success'};} res.end(JSON.stringify(data));
        });
            //log
            logEvent('info', 'customers',rowID+' deleted');
            break;
        case 'delSuppliers': db.suppliers.remove( { recID:rowID }, function(err, status) {
            if( err || !status ) { var data =  { 'status': 'fail'};
                //log
                logEvent('error', 'ERROR_LOG','unable to delete supplier ');
            }
            else{ var data =  { 'status': 'success'};} res.end(JSON.stringify(data));
        } );
            //log
            logEvent('info', 'suppliers',rowID+' deleted');
            break;
        case 'delEmployees': db.employees.remove( { recID:rowID }, function(err, status) {
            if( err || !status ) { var data =  { 'status': 'fail'};
                //log
                logEvent('error', 'ERROR_LOG','unable to delete employee ');
            }
            else{ var data =  { 'status': 'success'};} res.end(JSON.stringify(data));
        } );
            //log
            logEvent('info', 'employees',rowID+' deleted');
            break;
        case 'delTruckers': db.truckers.remove( { recID:rowID }, function(err, status) {
            if( err || !status ) { var data =  { 'status': 'fail'};
                //log
                logEvent('error', 'ERROR_LOG','unable to delete trucker ');
            }
            else{ var data =  { 'status': 'success'};} res.end(JSON.stringify(data));
        } );
            //log
            logEvent('info', 'truckers',rowID+' deleted');
            break;
        case 'delMaterials':db.materials.remove( { recID:rowID }, function(err, status) {
            if( err || !status ) { var data =  { 'status': 'fail'};
                //log
                logEvent('error', 'ERROR_LOG','unable to delete material ');
            }
            else{ var data =  { 'status': 'success'};} res.end(JSON.stringify(data));
        } );
            //log
            logEvent('info', 'materials',rowID+' deleted');
            break;
        case 'delEquipments':db.equipments.remove( { recID:rowID }, function(err, status) {
            if( err || !status ) { var data =  { 'status': 'fail'};
                //log
                logEvent('error', 'ERROR_LOG','unable to delete equipment ');
            }
            else{ var data =  { 'status': 'success'};} res.end(JSON.stringify(data));
        } );
            //log
            logEvent('info', 'equipments',rowID+' deleted');
            break;
        case 'delTruckTrailers': db.trucktrailers.remove( { recID:rowID }, function(err, status) {
            if( err || !status ) { var data =  { 'status': 'fail'};
                //log
                logEvent('error', 'ERROR_LOG','unable to delete truck trailer ');
            }
            else{ var data =  { 'status': 'success'};} res.end(JSON.stringify(data));
        } );
            //log
            logEvent('info', 'truck_trailers',rowID+' deleted');
            break;
        case 'delPaymentTerms': db.paymentterms.remove( { recID:rowID }, function(err, status) {
            if( err || !status ) { var data =  { 'status': 'fail'};
                //log
                logEvent('error', 'ERROR_LOG','unable to delete payment term ');
            }
            else{ var data =  { 'status': 'success'};} res.end(JSON.stringify(data));
        } );
            //log
            logEvent('info', 'paymentterms',rowID+' deleted');
            break;
        case 'delTaxDefinition': db.taxdefinition.remove( { recID:rowID }, function(err, status) {
            if( err || !status ) { var data =  { 'status': 'fail'};
                //log
                logEvent('error', 'ERROR_LOG','unable to delete tax definition ');
            }
            else{ var data =  { 'status': 'success'};} res.end(JSON.stringify(data));
        } );
            //log
            logEvent('info', 'taxdefinition',rowID+' deleted');
            break;
    }
}

/*=========================
 GET STATISTICS
 ==========================*/
function getStatistics(req, res){
    var databaseUrl = "mydb"; // "username:password@example.com/mydb"
    var tables = ["users","customers","suppliers","employees","truckers","materials","equipments","trucktrailers","paymentterms","taxdefinition","orders"]
    var db = require("mongojs").connect(databaseUrl, tables);

    var rowID = parseInt(req.body.rowID);

    switch(req.body.action){
        case 'orderStatistics': db.orders.count( { userID: { $exists: true } }, function(err, result) {
            if( err || !result ) { var data =  { 'status': 'fail'};
                //log
                logEvent('error', 'ERROR_LOG','unable to get orders statistics ');
            }
            else{ var data =  { 'status': 'success', 'stat':result};} res.end(JSON.stringify(data));
        });
            break;
        case 'customerStatistics': db.customers.count( { userID: { $exists: true } }, function(err, result) {
            if( err || !result ) { var data =  { 'status': 'fail'};
                //log
                logEvent('error', 'ERROR_LOG','unable to get customers statistics ');
            }
            else{ var data =  { 'status': 'success', 'stat':result};} res.end(JSON.stringify(data));
        });
            break;
        case 'supplierStatistics': db.suppliers.count( { userID: { $exists: true } }, function(err, result) {
            if( err || !result ) { var data =  { 'status': 'fail'};
                //log
                logEvent('error', 'ERROR_LOG','unable to get suppliers statistics ');
            }
            else{ var data =  { 'status': 'success', 'stat':result};} res.end(JSON.stringify(data));
        });
            break;
    }
}

/*=========================
 GENERATE XML
 ==========================*/
function generateXML(rootTitle, rootData){
    var et = require('elementtree');

    var XML = et.XML;
    var ElementTree = et.ElementTree;
    var element = et.Element;
    var subElement = et.SubElement;

    var date, root, dateCreated, resourceName, etree, xml;

    date = new Date();

    root = element(rootTitle);
    root.set('xmlns', 'http://');

    var index, value, varName;
    for (index in rootData) {
        varName = subElement(root, index);
        varName.text = rootData[index];
    }

    dateCreated = subElement(root, 'dateCreated');
    dateCreated.text = date;

    etree = new ElementTree(root);
    xml = etree.write({'xml_declaration': true});

    //write to file
    var fs = require('fs');
    fs.writeFile('../WEB-INF/xml/'+rootTitle+'.xml', xml, function (err) {
        if (err) {
            return console.log(err);
            //log
            logEvent('error', 'ERROR_LOG','unable to generate XML ');
        }else{
            console.log('xml file generated');
        }
    });

    return xml;
}


/*=========================
 SEND EMAIL
 ==========================*/
function sendEmail(mailSubject, mailMessage, mailAttachment, mailTo, mailFrom){
    var nodemailer = require("nodemailer");
    // create reusable transport method (opens pool of SMTP connections)
    var smtpTransport = nodemailer.createTransport("SMTP",{
        service: "Gmail",
        auth: {
            user: "minifiedapps@gmail.com",
            pass: "MinifiedApp$"
        }
    });

    var theAttachment = '';
    if(mailAttachment!=''){
        theAttachment = [{
            fileName: mailAttachment, // file on disk as an attachment
            filePath: '../WEB-INF/xml/'+mailAttachment // stream this file
        }];
    }

    smtpTransport.sendMail({
            host : "smtp.gmail.com", // smtp server hostname
            port : "465", // smtp server port
            ssl: true,	 // for SSL support – REQUIRES NODE v0.3.x OR HIGHER
            domain : "smtp.gmail.com", // domain used by client to identify itself to server
            to : mailTo,
            from : mailFrom,
            subject : mailSubject,
            html: mailMessage,
            attachments: theAttachment,
        },
        function(err, result){
            if(err){ console.log(err);
                //log
                logEvent('error', 'ERROR_LOG','unable to send email');
            }
        });

    console.log('Email Sent '+mailTo);

}

/*=========================
 EMAIL INVOICE
 ==========================*/
function emailTheInvoice(req, res){

    //get orders data
    var databaseUrl = "mydb";
    var tables = ["invoices","orders","customers","users"];
    var db = require("mongojs").connect(databaseUrl, tables);
    var rowID = parseInt(req.body.rowID);
    db.orders.find({ recID:rowID }, function(err, result) {
        var data = JSON.stringify(result);

        result.forEach(function(data){
            var mailMessage = '';
            mailMessage = mailMessage + "<table border='1' style='width:350px;border:1px solid #ccc;padding:10px;border-collapse:collapse;' cellpadding='3' bordercolor='' >";
            mailMessage = mailMessage + "<tr><td colspan='3'><div style='font-weight:bold;font-size:20px;color:#fff;background:#0088CC;padding:7px;'>Order No. : "+data.orderNo+"</div></td></tr>";
            mailMessage = mailMessage + "<tr><td colspan='3' style='font-size:18px;'><b>Customer Information</b></td></tr>";
            mailMessage = mailMessage + "<tr><td>Customer Name</td><td colspan='3'><b>"+data.customer+"</b></td></tr>";
            mailMessage = mailMessage + "<tr>	<td>Transaction Date</td><td><b>"+data.transactionDate+"</b></td>";
            mailMessage = mailMessage + "		<td rowspan='3' valign='top'>				";
            if(data.includePST==1){
                mailMessage = mailMessage + "			PST Amount: <b>"+data.pst_amount+"</b><br/><br/>	";
            }
            if(data.includeGST==1){
                mailMessage = mailMessage + "			GST Amount: <b>"+data.gst_amount+"</b><br/>";
            }
            mailMessage = mailMessage + "		</td>	";
            mailMessage = mailMessage + "</tr>";
            mailMessage = mailMessage + "<tr><td>Rate</td><td><b>"+data.customerRate+"</b></td></tr>";
            mailMessage = mailMessage + "<tr><td>Customer Price</td><td><b>"+data.customerPrice+"</b></td></tr>";
            mailMessage = mailMessage + "<tr><td>Slip Number</td><td colspan='2'><b>"+data.customerSlipNo+"</b> </td></tr>";

            mailMessage = mailMessage + "<tr><td colspan='3' style='font-size:20px;'><b>Other Information</b></td></tr>";
            mailMessage = mailMessage + "<tr><td>Material Type</td><td colspan='2'><b>"+data.materialType+"</b></td></tr>";
            mailMessage = mailMessage + "<tr><td>Material Quantity</td><td colspan='2'><b>"+data.materialQty+"</b></td></tr>";
            mailMessage = mailMessage + "<tr><td>Trucker </td><td colspan='2'><b>"+data.trucker+"</b></td></tr>";
            mailMessage = mailMessage + "<tr><td>Trucker Qnty.</td><td><b>"+data.truckerQty+"</b></td><td>Trucker Rate <b>"+data.truckerRate+"</b> </td></tr>";

            mailMessage = mailMessage + "<tr><td>Trucker Units</td><td><b>"+data.truckerUnit+"</b></td><td>Trucker Amt. <b>"+data.truckerAmount+"</b></td></tr>";
            mailMessage = mailMessage + "<tr><td>Discount Amount</td><td><b>"+data.discountAmount+"</b></td><td>Dispatch Fee <b>"+data.dispatchFee+"</b></td></tr>";
            if(data.includetruckerPST==1){
                mailMessage = mailMessage + "<tr><td>Trucker PST Amount: </td><td colspan='2'><b>"+data.truckerPST+"</b></td></tr>";
            }
            if(data.includetruckerGST==1){
                mailMessage = mailMessage + "<tr><td>Trucker GST Amount: </td><td colspan='2'><b>"+data.truckerGST+"</b></td></tr>";
            }

            // mailMessage = mailMessage + "<tr><td>Standby Time</td><td><b>"+data.standByTime+"</b></td>";
            // mailMessage = mailMessage + "		<td rowspan='3' valign='top' colspan='2'>";
            // mailMessage = mailMessage + "			 <b>"+data.customerHST+"</b>Customer HST<br/>";
            // mailMessage = mailMessage + "			 <b>"+data.supplierHST+"</b> Supplier HST<br/>";
            // mailMessage = mailMessage + "			 <b>"+data.truckerHST+"</b>Trucker HST<br/>";
            // mailMessage = mailMessage + "			 <b>"+data.includeUnionDues+"</b> Union Dues Included<br/>";
            // mailMessage = mailMessage + "			 <b>"+data.includeMaterialCost+"</b> Material Cost Included<br/>";
            // mailMessage = mailMessage + "		</td>";
            // mailMessage = mailMessage + "</tr>";
            mailMessage = mailMessage + "<tr><td>Delivery Price</td><td colspan='2'> <b>"+data.deliveryPrice+"</b></td></tr>";
            mailMessage = mailMessage + "<tr><td>Admin Amount</td><td colspan='2'><b>"+data.adminAmount+"</b></td></tr>";
            mailMessage = mailMessage + "<tr><td>Equipment Used</td><td colspan='2'><b>"+data.equipmentUsed+"</b></td></tr>";
            if(data.calculateDispatchFee==1){
                mailMessage = mailMessage + "<tr><td>Dispatch Fee: </td><td colspan='2'><b>"+data.dispatchFeeCalculated+"</b></td></tr>	";
            }
            if(data.includeunionDues==1){
                mailMessage = mailMessage + "<tr><td>Union Dues Amount: </td><td colspan='2'><b>"+data.unionDuesAmount+"</b></td></tr>	";
            }
            mailMessage = mailMessage + "</table>";

            var mailSubject = "Customer's Order";
            var mailAttachment = '';
            var mailTo = data.customer_email;
            var mailFrom = 'Apps & Pages <minifiedapps@gmail.com>';
            sendEmail(mailSubject, mailMessage, mailAttachment, mailTo, mailFrom);
        });

    });

    //update emai status
    db.orders.update( { recID: rowID },  { $set:  { email_status: '1'} }, { multi: true }, function(err, status) {
        if( err || !status ) { var data =  {'status': 'fail'};
            //log
            logEvent('error', 'ERROR_LOG','unable to update emai_status ');
        }
        else{ var data =  {'status': 'success'}; }
        res.end(JSON.stringify(data));
    });

}

/*=========================
 EMAIL ORDER
 ==========================*/
function emailOrder(req, res){

    //get orders data
    var databaseUrl = "mydb";
    var tables = ["orders"];
    var db = require("mongojs").connect(databaseUrl, tables);
    var rowID = parseInt(req.body.rowID);
    db.orders.find({ recID:rowID }, function(err, result) {
        var data = JSON.stringify(result);

        result.forEach(function(data){
            var mailMessage = '';
            mailMessage = mailMessage + "<img src='../WEB-INF/html/images/upload/1.png'><table border='1' style='width:350px;border:1px solid #ccc;padding:10px;border-collapse:collapse;' cellpadding='3' bordercolor='' >";
            mailMessage = mailMessage + "<tr><td colspan='3'><div style='font-weight:bold;font-size:20px;color:#fff;background:#0088CC;padding:7px;'>Order No. : "+data.orderNo+"</div></td></tr>";
            mailMessage = mailMessage + "<tr><td colspan='3' style='font-size:18px;'><b>Customer Information</b></td></tr>";
            mailMessage = mailMessage + "<tr><td>Customer Name</td><td colspan='3'><b>"+data.customer+"</b></td></tr>";
            mailMessage = mailMessage + "<tr>	<td>Transaction Date</td><td><b>"+data.transactionDate+"</b></td>";
            mailMessage = mailMessage + "		<td rowspan='3' valign='top'>				";
            if(data.includePST==1){
                mailMessage = mailMessage + "			PST Amount: <b>"+data.pst_amount+"</b><br/><br/>	";
            }
            if(data.includeGST==1){
                mailMessage = mailMessage + "			GST Amount: <b>"+data.gst_amount+"</b><br/>";
            }
            mailMessage = mailMessage + "		</td>	";
            mailMessage = mailMessage + "</tr>";
            mailMessage = mailMessage + "<tr><td>Rate</td><td><b>"+data.customerRate+"</b></td></tr>";
            mailMessage = mailMessage + "<tr><td>Customer Price</td><td><b>"+data.customerPrice+"</b></td></tr>";
            mailMessage = mailMessage + "<tr><td>Slip Number</td><td colspan='2'><b>"+data.customerSlipNo+"</b> </td></tr>";

            mailMessage = mailMessage + "<tr><td colspan='3' style='font-size:20px;'><b>Other Information</b></td></tr>";
            mailMessage = mailMessage + "<tr><td>Material Type</td><td colspan='2'><b>"+data.materialType+"</b></td></tr>";
            mailMessage = mailMessage + "<tr><td>Material Quantity</td><td colspan='2'><b>"+data.materialQty+"</b></td></tr>";
            mailMessage = mailMessage + "<tr><td>Trucker </td><td colspan='2'><b>"+data.trucker+"</b></td></tr>";
            mailMessage = mailMessage + "<tr><td>Trucker Qnty.</td><td><b>"+data.truckerQty+"</b></td><td>Trucker Rate <b>"+data.truckerRate+"</b> </td></tr>";

            mailMessage = mailMessage + "<tr><td>Trucker Units</td><td><b>"+data.truckerUnit+"</b></td><td>Trucker Amt. <b>"+data.truckerAmount+"</b></td></tr>";
            mailMessage = mailMessage + "<tr><td>Discount Amount</td><td><b>"+data.discountAmount+"</b></td><td>Dispatch Fee <b>"+data.dispatchFee+"</b></td></tr>";
            if(data.includetruckerPST==1){
                mailMessage = mailMessage + "<tr><td>Trucker PST Amount: </td><td colspan='2'><b>"+data.truckerPST+"</b></td></tr>";
            }
            if(data.includetruckerGST==1){
                mailMessage = mailMessage + "<tr><td>Trucker GST Amount: </td><td colspan='2'><b>"+data.truckerGST+"</b></td></tr>";
            }

            // mailMessage = mailMessage + "<tr><td>Standby Time</td><td><b>"+data.standByTime+"</b></td>";
            // mailMessage = mailMessage + "		<td rowspan='3' valign='top' colspan='2'>";
            // mailMessage = mailMessage + "			 <b>"+data.customerHST+"</b>Customer HST<br/>";
            // mailMessage = mailMessage + "			 <b>"+data.supplierHST+"</b> Supplier HST<br/>";
            // mailMessage = mailMessage + "			 <b>"+data.truckerHST+"</b>Trucker HST<br/>";
            // mailMessage = mailMessage + "			 <b>"+data.includeUnionDues+"</b> Union Dues Included<br/>";
            // mailMessage = mailMessage + "			 <b>"+data.includeMaterialCost+"</b> Material Cost Included<br/>";
            // mailMessage = mailMessage + "		</td>";
            // mailMessage = mailMessage + "</tr>";
            mailMessage = mailMessage + "<tr><td>Delivery Price</td><td colspan='2'> <b>"+data.deliveryPrice+"</b></td></tr>";
            mailMessage = mailMessage + "<tr><td>Admin Amount</td><td colspan='2'><b>"+data.adminAmount+"</b></td></tr>";
            mailMessage = mailMessage + "<tr><td>Equipment Used</td><td colspan='2'><b>"+data.equipmentUsed+"</b></td></tr>";
            if(data.calculateDispatchFee==1){
                mailMessage = mailMessage + "<tr><td>Dispatch Fee: </td><td colspan='2'><b>"+data.dispatchFeeCalculated+"</b></td></tr>	";
            }
            if(data.includeunionDues==1){
                mailMessage = mailMessage + "<tr><td>Union Dues Amount: </td><td colspan='2'><b>"+data.unionDuesAmount+"</b></td></tr>	";
            }
            mailMessage = mailMessage + "</table>";

            var mailSubject = "Customer's Order";
            var mailAttachment = '';
            var mailTo = data.customer_email;
            var mailFrom = 'Apps & Pages <minifiedapps@gmail.com>';
            sendEmail(mailSubject, mailMessage, mailAttachment, mailTo, mailFrom);
        });

    });

    //update emai status
    db.orders.update( { recID: rowID },  { $set:  { email_status: '1'} }, { multi: true }, function(err, status) {
        if( err || !status ) { var data =  {'status': 'fail'};
            //log
            logEvent('error', 'ERROR_LOG','unable to update emai_status ');
        }
        else{ var data =  {'status': 'success'}; }
        res.end(JSON.stringify(data));
    });

}

/*=========================
 GENERATE CODE
 ==========================*/
function generateCode()
{
    var alphanum = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    var code = '';
    for( var i=0; i < 7; i++ ){
        code += alphanum.charAt(Math.floor(Math.random() * alphanum.length));
    }
    return code;
}

/*=========================
 GENERATE ORDER NUMBER
 ==========================*/
function generateOrderNo(recID)
{
    var recID = new String(recID);
    var orderNo = '';
    switch(recID.length){
        case 1: orderNo = "000000000"+recID; break;
        case 2: orderNo = "00000000"+recID; break;
        case 3: orderNo = "0000000"+recID; break;
        case 4: orderNo = "000000"+recID; break;
        case 5: orderNo = "00000"+recID; break;
        case 6: orderNo = "0000"+recID; break;
        case 7: orderNo = "000"+recID; break;
        case 8: orderNo = "00"+recID; break;
        case 9: orderNo = "0"+recID; break;
        default: orderNo = recID;
    }
    return String(orderNo);
}




