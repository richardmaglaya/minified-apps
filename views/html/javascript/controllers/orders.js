var app = angular.module('MyApp', ['ngGrid']);
app.controller('OrderCtrl', function($scope) {
	
	
	$.post("/getData", {action:'getOrders'}, function(result){
				$scope.myData = [];
				result.forEach(function(db){	
					if(db.email_status==1){ var status = 'done';}else{ var status='draft'}
					var actionLink = db.recID;
						var format = { 													
								orderNo : db.orderNo,
								customer : db.customer,		
								supplier : db.supplier,	
								equipmentUsed : db.equipmentUsed,	
								transactionDate : db.transactionDate,
								email_status : status,
								action : actionLink								
							}
					$scope.myData.push(format);						
				});
			console.log($scope.myData);							
		});
		
		// alert('Success!');
		$scope.gridOptions = { data: 'myData' };
	
					 
		 $scope.myDefs = [
        { field: 'orderNo', displayName: 'Order No.', width: 130 },
		{ field: 'customer', displayName: 'Customer', width: 150 },
        { field: 'supplier', displayName: 'Supplier', width: 150 },
		{ field: 'equipmentUsed', displayName: 'Equipment Used', width: 100 },
		{ field: 'transactionDate', displayName: 'Date', width: 100 },        
		
		{ field: 'email_status', displayName: 'Status', width: 80 },
		
		{ field: 'action', displayName: 'Action', enableCellEdit: false, cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()" ><a class="btn btn-primary btn-mini" href="javascript:void(0)" onclick="viewOrder({{row.getProperty(col.field)}})"><i class="icon-eye-open icon-white"></i> VIEW</a>&nbsp;<a class="btn btn-warning btn-mini" href="javascript:void(0)" onclick="editOrder({{row.getProperty(col.field)}})"><i class="icon-pencil icon-white"></i> EDIT</a>&nbsp;<a class="btn btn-danger btn-mini" href="javascript:void(0)" onclick="delOrder({{row.getProperty(col.field)}})"><i class="icon-trash icon-white"></i> DEL</a></div>' }
        
		];
		
		 $scope.gridOptions = {
			data: 'myData',
			enableColumnResize: true,
			enableColumnReordering: true,
			selectedItems: $scope.mySelections,
			headerRowHeight: 40,
			pagingOptions: $scope.pagingOptions,
			enablePaging: true,
			enableRowSelection: true,
			multiSelect: false,
			enableRowReordering: true,
			enablePinning: true,
			showGroupPanel: true,
			showFooter: true,
			showFilter: true,
			enableCellEdit: false,
			enableCellSelection: true,
			showColumnMenu: true,
			maintainColumnRatios: true,
			columnDefs: 'myDefs',
			primaryKey: 'id',
			sortInfo: {fields:['invoice_date'], directions:['desc'] }
		};
			
    
});
		//ACTION BUTTONS
		function viewOrder(rowID){			
			window.location.href = 'view_order.html?rowID='+rowID;	
		}
		
		function editOrder(rowID){			
			window.location.href = 'edit_order.html?rowID='+rowID;	
		}
		
		function delOrder(rowID){
			if (confirm("Are you sure you want to remove this?")){				
				$.post("/removeData", {action: 'delOrders', rowID:rowID}, function(data){
					if(data.status=='success'){
						alert('Delete Successful!');	
						window.location.reload(true);
					}
			   });
			}
		}