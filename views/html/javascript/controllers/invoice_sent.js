var app = angular.module('MyApp', ['ngGrid']);
app.controller('InvoiceSentCtrl', function($scope) {	
	
	$.post("/getData", {action:'getInvoices', mode:'sent'}, function(result){
				$scope.myData = [];
				result.forEach(function(db){				
					var actionLink = db.recID;
						var format = { 
								invoice_num : db.invoice_number,
								customer : db.customerName,		
								amount : db.invoiceAmount,	
								invoice_date : db.invoiceDate,									
								action : actionLink								
							}
					$scope.myData.push(format);						
				});
			console.log($scope.myData);							
		});
		
		// alert('Success!');
		$scope.gridOptions = { data: 'myData' };
	
					 
		 $scope.myDefs = [
        { field: 'invoice_num', displayName: 'Invoice No.', width: 130 },
		{ field: 'customer', displayName: 'Customer', width: 200 },
        { field: 'amount', displayName: 'Amount', width: 150 },
		{ field: 'invoice_date', displayName: 'Date', width: 150 },        		
		{ field: 'action', displayName: 'Action', enableCellEdit: false, cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()" ><a class="btn btn-primary btn-mini" href="javascript:void(0)" onclick="viewInvoice({{row.getProperty(col.field)}})"><i class="icon-eye-open icon-white"></i> VIEW</a>&nbsp;<a class="btn btn-warning btn-mini" href="javascript:void(0)" onclick="editInvoice({{row.getProperty(col.field)}})"><i class="icon-pencil icon-white"></i> EDIT</a>&nbsp;<a class="btn btn-danger btn-mini" href="javascript:void(0)" onclick="delInvoice({{row.getProperty(col.field)}})"><i class="icon-trash icon-white"></i> DEL</a></div>' }
      
		];
		
		 $scope.gridOptions = {
			data: 'myData',
			enableColumnResize: true,
			enableColumnReordering: true,
			selectedItems: $scope.mySelections,
			headerRowHeight: 40,
			pagingOptions: $scope.pagingOptions,
			enablePaging: true,
			enableRowSelection: true,
			multiSelect: false,
			enableRowReordering: true,
			enablePinning: true,
			showGroupPanel: true,
			showFooter: true,
			showFilter: true,
			enableCellEdit: false,
			enableCellSelection: true,
			showColumnMenu: true,
			maintainColumnRatios: true,
			columnDefs: 'myDefs',
			primaryKey: 'id',
			sortInfo: {fields:['invoice_date'], directions:['desc'] }
		};
		    
});
		//ACTION BUTTONS
		function viewInvoice(rowID){			
			window.location.href = 'view_invoice.html?rowID='+rowID;	
		}
		
		function editInvoice(rowID){			
			window.location.href = 'edit_invoice.html?rowID='+rowID;	
		}
		
		function delInvoice(rowID){
			if (confirm("Are you sure you want to remove this?")){				
				$.post("/removeData", {action: 'delInvoices', rowID:rowID}, function(data){
					if(data.status=='success'){
						alert('Delete Successful!');	
						window.location.reload(true);
					}
			   });
			}
		}