var foo = angular.module('bar',[])

.directive('leanTest', ['$rootScope', 'HelperService', 'EmailService', function($rootScope, HelperService, EmailService) {
  return {
    restrict: 'A',
    link:function (scope, element, attrs) {
      var done = false;

      element.click(function() {
        event.stopPropagation();

        // make sure this only happens once
        if(!done) {
          // disable the button
          element.addClass('disabled');
          done = true;

          // Send an email with the Customer.IO JS SDK
          var email = '';
          email = $rootScope.currentUser.get('email');

          if(typeof email === "undefined") {
            email = "Anonymous User";
          } else {
            email = $rootScope.currentUser.get('email');
          }
          $rootScope.currentUser.get('email');
          // Track in your favourite analytics tool
          HelperService.metrics.track(attrs.leanTest, {user: email, page: location.href});

          // Send an email
          EmailService.sendLeanTest(attrs.leanTest, email);
        }

      });

    }
  }
}])
