function registrationCtrl($scope) {
	$scope.customer_number = '';
	$scope.customer_name = '';
	$scope.address = '';
	$scope.city = '';
	$scope.postal_code = '';
	$scope.gst_number = '';
	$scope.email_address = '';
	$scope.work_num = '';
	$scope.home_num = '';
	$scope.mobile_number = '';
	$scope.faxNumber = '';
	$scope.wcb_num = '';
	$scope.customer_number = '';
	$scope.customer_number = '';
	$scope.customer_number = '';
	
	$scope.submitReg = function() {
		//generate xml 
		// richardmaglaya@gmail.com
	}
	
	$scope.cancelReg = function() {
		$scope = '';
	}
	
}

function signUpCtrl($scope, $http) {
	$scope.firstName = '';
	$scope.lastName = '';
	$scope.email = '';
	$scope.emailConfirm = '';
	$scope.password = '';
	$scope.mobile_number = '';
	
	$scope.submitSignUp = function() {		
		$.post("/signUp", {
				firstName:$scope.firstName,
				lastName:$scope.lastName,
				email:$scope.email,
				password:$scope.password,
            mobile_number:$scope.mobile_number
				}, function(data){					
					if(data.status=='success'){
						// add success code here
					}else{
						alert('Ups! Please try again later...');
					}				
		});	
	}
	
	$scope.cancelSignUp = function() {
		//redirect to main page		
		window.location = 'index.html'
	}	
}

function customerRegistryCtrl($scope) {
	$scope.submitReg = function() {
		$.post("/customerRegistry", {
			customer_number:$scope.customer_number,
			customer_name:$scope.customer_name,
			address:$scope.address,
			province:$scope.province,
			city:$scope.city,
			postal_code:$scope.postal_code,
			gst_number:$scope.gst_number,
			email_address:$scope.email_address,
			work_num:$scope.work_num,
			home_num:$scope.home_num,
            mobile_number:$scope.mobile_number,
			faxNumber:$scope.faxNumber,
			payment_term:$scope.payment_term,
			wcb_num:$scope.wcb_num
			}, function(data){					
					if(data.status=='success'){
						// add success code here
					}else{
						alert('Ups! Please try again later...');
					}				
		});	
	}	
	$scope.cancelReg = function() {
		//redirect to main page		
		window.location = 'index.html'
	}	
}

function supplierRegistryCtrl($scope) {
	$scope.submitReg = function() {
		$.post("/supplierRegistry", {
			customer_number:$scope.customer_number,
			customer_name:$scope.customer_name,
			address:$scope.address,
			province:$scope.province,
			city:$scope.city,
			postal_code:$scope.postal_code,
			gst_number:$scope.gst_number,
			email_address:$scope.email_address,
			work_num:$scope.work_num,
			home_num:$scope.home_num,
            mobile_number:$scope.mobile_number,
			faxNumber:$scope.faxNumber,
			payment_term:$scope.payment_term,
			wcb_num:$scope.wcb_num
			}, function(data){					
					if(data.status=='success'){
						// add success code here
					}else{
						alert('Ups! Please try again later...');
					}				
		});	
	}
	$scope.cancelReg = function() {
		//redirect to main page		
		window.location = 'index.html'
	}	
}

function employeeRegistryCtrl($scope) {
	$scope.submitReg = function() {
		$.post("/employeeRegistry", {
			customer_number:$scope.customer_number,
			customer_name:$scope.customer_name,
			address:$scope.address,
			province:$scope.province,
			city:$scope.city,
			postal_code:$scope.postal_code,
			gst_number:$scope.gst_number,
			email_address:$scope.email_address,
			work_num:$scope.work_num,
			home_num:$scope.home_num,
            mobile_number:$scope.mobile_number,
			faxNumber:$scope.faxNumber,
			payment_term:$scope.payment_term,
			wcb_num:$scope.wcb_num,
			sin_ssn:$scope.sin_ssn
			}, function(data){					
					if(data.status=='success'){
						// add success code here
					}else{
						alert('Ups! Please try again later...');
					}				
		});	
	}
	$scope.cancelReg = function() {
		//redirect to main page		
		window.location = 'index.html'
	}	
}

function truckerRegistryCtrl($scope) {
	$scope.submitReg = function() {
		$.post("/truckerRegistry", {
			customer_number:$scope.customer_number,
			customer_name:$scope.customer_name,
			address:$scope.address,
			province:$scope.province,
			city:$scope.city,
			postal_code:$scope.postal_code,
			gst_number:$scope.gst_number,
			email_address:$scope.email_address,
			work_num:$scope.work_num,
			home_num:$scope.home_num,
            mobile_number:$scope.mobile_number,
			faxNumber:$scope.faxNumber,
			payment_term:$scope.payment_term,
			wcb_num:$scope.wcb_num,
			sin_ssn:$scope.sin_ssn
			}, function(data){					
					if(data.status=='success'){
						// add success code here
					}else{
						alert('Ups! Please try again later...');
					}				
		});	
	}
	$scope.cancelReg = function() {
		//redirect to main page		
		window.location = 'index.html'
	}	
}

function materialsDefinitionCtrl($scope) {	
	$scope.saveForm = function() {
		$.post("/materialsDefinition", {
			material_num:$scope.material_num,
			material_name:$scope.material_name,				
			add_info:$scope.add_info
			}, function(data){					
					if(data.status=='success'){
						// add success code here
						alert('Email Sent!');
					}else{
						alert('Ups! Please try again later...');
					}				
		});		
	}
	
	$scope.closeForm = function() {
		//redirect to main page		
		window.location = 'index.html'
	}	
}

function taxCtrl($scope) {
	$scope.save = function() {
		$.post("/taxDefinition", {
			federal_tax:$scope.federal_tax,
			local_tax:$scope.local_tax,				
			union_dues:$scope.union_dues,
			dispatch_fee:$scope.dispatch_fee
			}, function(data){					
					if(data.status=='success'){					
						// add success code here
						alert('Email Sent!');
					}else{
						alert('Ups! Please try again later...');
					}				
		});	
	}	
	$scope.close = function() {
		//redirect to main page		
		window.location = 'index.html'
	}	
}

function paymentTermsDefinitionCtrl($scope) {
	$scope.save = function() {
		$.post("/paymentTermDefinition", {
			terms_num:$scope.terms_num,
			terms_name:$scope.terms_name,				
			add_info:$scope.add_info
			}, function(data){					
					if(data.status=='success'){
						// add success code here
					}else{
						alert('Ups! Please try again later...');
					}				
		});	
	}
	$scope.close = function() {
		//redirect to main page		
		window.location = 'index.html'
	}
}
 
 function truckTrailerDefinitionCtrl($scope) {
	$scope.save = function() {
		$.post("/truckTrailerDefinition", {
			truck_num:$scope.truck_num,
			year_and_make:$scope.year_and_make,				
			vin:$scope.vin
			}, function(data){					
					if(data.status=='success'){
						// add success code here
					}else{
						alert('Ups! Please try again later...');
					}				
		});	
	}
	$scope.close = function() {
		//redirect to main page		
		window.location = 'index.html'
	}
}
  
