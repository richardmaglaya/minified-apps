$(document).ready(function() {	
	
	$.post("/isUserLogged", function(data){						
		 if(data.status=='yes') { 
			$('#signinFrame').hide();
			$('#accountName').html('Welcome '+data.firstName);
			$('#open').attr('id','logout');
			$('#logout').html('Log Out'); $('#logout').css('display','block');
			$('#close').css('display','none');							
			$("div#panel").animate({height: 35}, 'medium').css('background','#ededed');
		 }				 					  
	});	
	
	$("div#panel").slideDown(1000);
	$("#toggle a").toggle();
	// Expand Panel
	$("#open").click(function(){
		$("div#panel").animate({height: 115}, 'slow').css('background','');	
		$("#signinFrame").fadeIn();
		//$("div#panel").slideDown("slow");	
	});	
	
	// Collapse Panel
	$("#close").click(function(){
		$("#signinFrame").fadeOut();
		$("div#panel").animate({height: 35}, 'medium').css('background','#ededed');		
	//	$("div#panel").slideUp("slow");	
	});		
	
	// Switch buttons from "Log In | Register" to "Close Panel" on click
	$("#toggle a").click(function () {
		$("#toggle a").toggle();
	});	
	
	//SIGN IN 
	$('#signin').click(function(){
			var email = $('#email').val();
			var password = $('#password').val();
			$.post("/loginUser", {email:email, password: password}, function(data){						
				 if(data.email.length==0) {
                     alert('Invalid username and password.');
				 }else{						
					$('#signinFrame').hide();
					$('#accountName').html('Welcome '+data.firstName);
					$('#open').attr('id','logout');
					$('#logout').html('Log Out'); $('#logout').css('display','block');
					$('#close').css('display','none');							
					$("div#panel").animate({height: 35}, 'medium').css('background','#ededed');
					
				 }					 					  
			});				
	});
	
	$('#logout').live('mousedown',function(){
		$.post("/logoutUser", function(data){						
			 if(data.status=='success') { 
				alert('Log Out Successful!');
				window.location.reload(true);
			 }else{						
				alert('Log Out Failed! Error in connection.');				
			 }					 					  
		});	
	});


/*
	$("div#panel").slideDown(1000);
	//$("#toggle a").toggle();
	$("div#panel").animate({height: 35}, 'fast').css('background','#ededed');
	$("#signin_Frame").fadeOut();
	// Expand Panel
	$("#open").click(function(){
		$("#signin_Frame").fadeIn();
		$("div#panel").animate({height: 115}, 'slow').css('background','');	
	});	
	
	// Collapse Panel
	$("#close").click(function(){		
		$("div#panel").animate({height: 35}, 'medium').css('background','#ededed');
		$("#signin_Frame").fadeOut();
	});		
	
	// Switch buttons from "Sign In | Sign Up" to "Close" on click
	$("#toggle a").click(function () {
		$("#toggle a").toggle();
	});		
		
	$('#signin').click(function() {		
		var email = $('#email').val();
		var password = $('#password').val();
		$('#email').val(''); $('#email').val('');
		// alert('sign in  '+email+' '+password);
		// $("#log_result").html("<img src=\"<?php echo url::site()?>assets/images/ajax-loader.gif\">processing...");
		// $.post("<?php  echo url::site()?>index/login", {user: user, pass:pass}, function(data){			
			// if(data.status==0){
				// $('#log_result').html("<span >Ups! Please check your username and password and try again.</span>");	
			// }else{
				// $('#log_result').html("UserID : "+data.userID+"<br/>Password : "+data.password+"<br/>Status : "+data.status);	
				// $('#user').val('');	$('#pass').val('');
				// $('#login_form').slideUp();				
			// }			
		  // }, 'json');
		
		$("div#panel").animate({height: 35}, 'medium').css('background','#eee');
		$("#signin_Frame").fadeOut();
		  $("#toggle a").toggle();
		  $("#toggle a#open").attr('id','sign_out').html('Sign Out');
		  $("#account_name").css('display','block').html('active account:'+email);
	});
	
	$('#sign_out').live('mousedown',(function() {			
		$("#toggle a#sign_out").attr('id','open').html('Sign In');
		$("#account_name").animate('slow').css('display','none').html('');	
	}));
	
	$('#sign_up').click(function() {				
		closeTab();
		$('div#content-main').load('reg.htm');
		
		
	});
	
	$('#tracking').click(function() {		
		alert('load the tracking page');
		
	});
	
	$('#invoice').click(function() {		
		alert('load the invoice page');
		
	});	
	*/
});

function closeTab(){
	$("div#panel").animate({height: 35}, 'medium').css('background','#eee');
	$("#signin_Frame").fadeOut();
}